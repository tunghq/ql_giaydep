﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;
using System.Data.SqlClient;
namespace QL_GiayDep
{
    public partial class frmBaoCaoHDBtheoNV : Form
    {
        public frmBaoCaoHDBtheoNV()
        {
            InitializeComponent();
        }

        private void frmBaoCaoHDBtheoNV_Load(object sender, EventArgs e)
        {

            this.reportViewer1.RefreshReport();
            //LOAD Mã NV
            using (SqlConnection sqlConnection = Connection.GetSqlConnection())
            {
                sqlConnection.Open();
                SqlDataAdapter da = new SqlDataAdapter("Select* from dbo.NhanVien where MaCV like 'CV02'", sqlConnection);//SQL là câu truy vấn bảng trong cơ sở dữ liệu, cn là connection đến cơ sở dữ liệu
                DataTable dt = new DataTable();
                da.Fill(dt);
                comboBox1.DisplayMember = "MaNV";//Word là tên trường bạn muốn hiển thị trong combobox
                comboBox1.ValueMember = "MaNV";
                comboBox1.DataSource = dt;
                sqlConnection.Close();
            }
        }
        Modify modify = new Modify();
        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            string sql = "exec dbo.HDBtuNV N'" + comboBox1.Text + "'";
            try
            {

                reportViewer1.LocalReport.ReportEmbeddedResource = "QL_GiayDep.Report3.rdlc";
                ReportDataSource reportDataSource = new ReportDataSource();
                reportDataSource.Name = "DataSet1";
                reportDataSource.Value = modify.Table(sql);
                reportViewer1.LocalReport.DataSources.Add(reportDataSource);
                this.reportViewer1.RefreshReport();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            
        }
    }
}
