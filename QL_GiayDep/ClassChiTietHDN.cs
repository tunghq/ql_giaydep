﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QL_GiayDep
{
    class ClassChiTietHDN
    {
        private string __mahdn;
        private string __masp;
        private int __soluong;
        private double __giamgia;

        public ClassChiTietHDN()
        {
        }

        public ClassChiTietHDN(string mahdn, string masp, int soluong, double giamgia)
        {
            Mahdn = mahdn;
            Masp = masp;
            Soluong = soluong;
            Giamgia = giamgia;
        }

        public string Mahdn { get => __mahdn; set => __mahdn = value; }
        public string Masp { get => __masp; set => __masp = value; }
        public int Soluong { get => __soluong; set => __soluong = value; }
        public double Giamgia { get => __giamgia; set => __giamgia = value; }
    }
}
