﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
namespace QL_GiayDep
{
    public partial class frmHoaDonNhap : Form
    {
        Modify modify;
        ClassHoaDonNhap hoaDonNhap;
        public frmHoaDonNhap()
        {
            InitializeComponent();
        }

        private void frmHoaDonNhap_Load(object sender, EventArgs e)
        {
            string query = "select *from dbo.HoaDonNhap";
            //LOAD DATABASE HOADON
            modify = new Modify();
            try
            {
                dataGridView1.DataSource = modify.Table(query);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Lỗi: " + ex.Message);
            }
            //LOAD Mã NV
            using (SqlConnection sqlConnection = Connection.GetSqlConnection())
            {
                sqlConnection.Open();
                SqlDataAdapter da = new SqlDataAdapter("Select* from dbo.NhanVien where MaCV like 'CV01'", sqlConnection);//SQL là câu truy vấn bảng trong cơ sở dữ liệu, cn là connection đến cơ sở dữ liệu
                DataTable dt = new DataTable();
                da.Fill(dt);
                cbMaNV.DisplayMember = "MaNV";//Word là tên trường bạn muốn hiển thị trong combobox
                cbMaNV.ValueMember = "MaNV";
                cbMaNV.DataSource = dt;
                sqlConnection.Close();
            }
            //LOAD Mã NCC
            using (SqlConnection sqlConnection = Connection.GetSqlConnection())
            {
                sqlConnection.Open();
                SqlDataAdapter da = new SqlDataAdapter("Select* from dbo.NhaCungCap ", sqlConnection);//SQL là câu truy vấn bảng trong cơ sở dữ liệu, cn là connection đến cơ sở dữ liệu
                DataTable dt = new DataTable();
                da.Fill(dt);
                cbMaNCC.DisplayMember = "MaNCC";//Word là tên trường bạn muốn hiển thị trong combobox
                cbMaNCC.ValueMember = "MaNCC";
                cbMaNCC.DataSource = dt;
                sqlConnection.Close();
            }
            Clear_TextBoxes();
            Disable_Textboxes();
            Disable_2Buttons();
            Enable_3Buttons();
        }
        private void Disable_Textboxes()
        {
            txtMaHDN.ReadOnly = true;
            cbMaNCC.Enabled = false;
            cbMaNV.Enabled = false;
        }
        private void Enable_Textboxes()
        {
            txtMaHDN.ReadOnly = false;
            cbMaNCC.Enabled = true;
            cbMaNV.Enabled = true ;
        }
        private void Enable_2Buttons()
        {
            btnHuy.Enabled = true;
            btnLuu.Enabled = true;
        }
        private void Disable_2Buttons()
        {
            btnHuy.Enabled = false;
            btnLuu.Enabled = false;
        }
        private void Enable_3Buttons()
        {
            btnThem.Enabled = true;
            btnSua.Enabled = true;
            btnXoa.Enabled = true;
        }
        private void Clear_TextBoxes()
        {
            txtMaHDN.Text = "";
            cbMaNV.Text = "";
            cbMaNCC.Text = "";
        }
        private bool CheckTextBoxesHDN()
        {
            if (txtMaHDN.Text.Trim() == "")//su dung trim khong lay khoang trang giua cac ki tu
            {
                MessageBox.Show("Vui lòng nhập mã hóa đơn nhập");
                return false;
            }
            return true;
        }
        private void getValueHDN()
        {
            string mahdn = this.txtMaHDN.Text.Trim();
            string manv = this.cbMaNV.Text.Trim();
            string ngaynhap = dtpNgayNhap.Value.ToShortDateString();
            string mancc = this.cbMaNCC.Text.Trim();
            hoaDonNhap = new ClassHoaDonNhap(mahdn, manv, ngaynhap, mancc);
        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            Enable_2Buttons();
            Enable_Textboxes();
            btnXoa.Enabled = false;
            btnSua.Enabled = false;
        }

        private void btnSua_Click(object sender, EventArgs e)
        {

            Enable_Textboxes();
            Enable_2Buttons();
            btnThem.Enabled = false;
            btnXoa.Enabled = false;
            txtMaHDN.ReadOnly = true;
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {

            Disable_Textboxes();
            Enable_2Buttons();
            btnThem.Enabled = false;
            btnSua.Enabled = false;
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            if (CheckTextBoxesHDN())
            {
                string query = "";
                if (!btnSua.Enabled && !btnXoa.Enabled)
                {
                    getValueHDN();
                    query = "INSERT[dbo].[HoaDonNhap]([MaHDN], [NgayNhap], [MaNCC], [MaNV],[TongTien]) VALUES(N'" + hoaDonNhap.Mahdn + "', N'" + hoaDonNhap.Ngaynhap + "', N'" + hoaDonNhap.Mancc + "', N'" + hoaDonNhap.Manv + "', NULL)";
                    try
                    {
                        if (MessageBox.Show("Bạn có muốn lưu không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
                        {
                            modify.setTable(query);
                            Clear_TextBoxes();
                            Enable_Textboxes();
                            MessageBox.Show("Thêm vào thành công");
                            frmHoaDonNhap_Load(sender, e);
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("lỗi " + ex.Message);
                    }
                }
                if (!btnThem.Enabled && !btnXoa.Enabled)
                {
                    getValueHDN();
                    query = "Update dbo.HoaDonNhap SET ";
                    query += "NgayNhap = '" + dtpNgayNhap.Value.Date + "',";
                    query += "MaNCC = '" + cbMaNCC.Text + "',";
                    query += "MaNV = N'" + cbMaNV.Text + "'";
                    query += "Where MaHDN like N'" + txtMaHDN.Text + "'";
                    try
                    {
                        if (MessageBox.Show("Bạn có muốn lưu không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
                        {
                            modify.setTable(query);
                            Clear_TextBoxes();
                            Enable_Textboxes();
                            MessageBox.Show("Sửa thành công");
                            frmHoaDonNhap_Load(sender, e);
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("lỗi " + ex.Message);
                    }
                }
                if (!btnThem.Enabled && !btnSua.Enabled)
                {
                    getValueHDN();
                    query = "Delete From dbo.HoaDonNhap Where MaHDN =N'" + txtMaHDN.Text + "'";
                    try
                    {
                        if (MessageBox.Show("Bạn có muốn lưu không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
                        {
                            modify.setTable(query);
                            MessageBox.Show("Xóa thành công");
                            frmHoaDonNhap_Load(sender, e);
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("lỗi " + ex.Message);
                    }
                }
            }
        }

        private void btnHuy_Click(object sender, EventArgs e)
        {
            frmHoaDonNhap_Load(sender, e);
        }

        private void btnLamMoi_Click(object sender, EventArgs e)
        {
            frmHoaDonNhap_Load(sender, e);
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                int i = dataGridView1.CurrentRow.Index;
                txtMaHDN.Text = dataGridView1.Rows[i].Cells[0].Value.ToString();
                cbMaNV.Text = dataGridView1.Rows[i].Cells[1].Value.ToString();
                dtpNgayNhap.Text = dataGridView1.Rows[i].Cells[2].Value.ToString();
                cbMaNCC.Text = dataGridView1.Rows[i].Cells[3].Value.ToString();

            }
            catch (Exception ex)
            {
                MessageBox.Show("lỗi " + ex.Message);
            }
        }

        private void btnThoat_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnThemCTHD_Click(object sender, EventArgs e)
        {
            frmChiTietHDN f = new frmChiTietHDN();
            f.Show();
        }
    }
}
