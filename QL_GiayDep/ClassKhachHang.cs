﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QL_GiayDep
{
    class ClassKhachHang
    {
        private string __makh;
        private string __tenkh;
        private string __diachi;
        private string __sdt;

        public ClassKhachHang()
        {
        }

        public ClassKhachHang(string makh, string tenkh, string diachi, string sdt)
        {
            Makh = makh;
            Tenkh = tenkh;
            Diachi = diachi;
            Sdt = sdt;
        }

        public string Makh { get => __makh; set => __makh = value; }
        public string Tenkh { get => __tenkh; set => __tenkh = value; }
        public string Diachi { get => __diachi; set => __diachi = value; }
        public string Sdt { get => __sdt; set => __sdt = value; }
    }
}
