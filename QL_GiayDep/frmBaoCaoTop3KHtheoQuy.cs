﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;

namespace QL_GiayDep
{
    public partial class frmBaoCaoTop3KHtheoQuy : Form
    {
        public frmBaoCaoTop3KHtheoQuy()
        {
            InitializeComponent();
        }

        private void frmBaoCaoTop3KHtheoQuy_Load(object sender, EventArgs e)
        {

            this.reportViewer1.RefreshReport();
        }
        Modify modify = new Modify();
        private void button1_Click(object sender, EventArgs e)
        {
            string sql = "exec dbo.TOP3KHtheoQuy " + comboBox1.Text + "";
            try
            {

                reportViewer1.LocalReport.ReportEmbeddedResource = "QL_GiayDep.Report4.rdlc";
                ReportDataSource reportDataSource = new ReportDataSource();
                reportDataSource.Name = "DataSet1";
                reportDataSource.Value = modify.Table(sql);
                reportViewer1.LocalReport.DataSources.Add(reportDataSource);
                this.reportViewer1.RefreshReport();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
