﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QL_GiayDep
{
   
    public partial class frmKhachHang : Form
    {
        Modify modify;
        ClassKhachHang khachHang;
        public frmKhachHang()
        {
            InitializeComponent();
        }

        private void frmKhachHang_Load(object sender, EventArgs e)
        {
            modify = new Modify();
            string query = "Select *from dbo.KhachHang";
            try
            {
                dataGridView1.DataSource = modify.Table(query);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Lỗi" + ex);
            }
            Clear_TextBoxes();
            Disable_Textboxes();
            Disable_2Buttons();
            Enable_3Buttons();
        }
        private void Disable_Textboxes()
        {
            txtMaKH.ReadOnly = true;
            txtTenKH.ReadOnly = true;
            txtSDT.ReadOnly = true;
            txtDiaChi.ReadOnly = true;
        }
        private void Enable_Textboxes()
        {
            txtMaKH.ReadOnly = false;
            txtTenKH.ReadOnly = false;
            txtSDT.ReadOnly = false;
            txtDiaChi.ReadOnly = false;
        }
        private void Enable_2Buttons()
        {
            btnHuy.Enabled = true;
            btnLuu.Enabled = true;
        }
        private void Disable_2Buttons()
        {
            btnHuy.Enabled = false;
            btnLuu.Enabled = false;
        }
        private void Enable_3Buttons()
        {
            btnThem.Enabled = true;
            btnSua.Enabled = true;
            btnXoa.Enabled = true;
        }
        private void Clear_TextBoxes()
        {
            txtMaKH.Text = "";
            txtTenKH.Text = "";
            txtSDT.Text = "";
            txtDiaChi.Text = "";
        }
        private bool CheckTextBoxes()
        {
            if (txtMaKH.Text.Trim() == "")//su dung trim khong lay khoang trang giua cac ki tu
            {
                MessageBox.Show("Vui lòng nhập mã khách hàng");
                return false;
            }
            if (txtTenKH.Text.Trim() == "")//su dung trim khong lay khoang trang giua cac ki tu
            {
                MessageBox.Show("Vui lòng nhập tên khách hàng");
                return false;
            }
            if (txtDiaChi.Text.Trim() == "")//su dung trim khong lay khoang trang giua cac ki tu
            {
                MessageBox.Show("Vui lòng nhập địa chỉ của khách hàng");
                return false;
            }
            if (txtSDT.Text.Trim() == "" || txtSDT.Text.Length < 10 || txtSDT.Text.Length > 10)//su dung trim khong lay khoang trang giua cac ki tu
            {
                MessageBox.Show("Vui lòng nhập số điện thoại liên lạc(SDT 10 chữ số)");
                return false;
            }
            return true;
        }
        private void getValueTextBoxes()
        {

            string makh = this.txtMaKH.Text.Trim();
            string tenkh = this.txtTenKH.Text.Trim();
            string diachi = this.txtDiaChi.Text.Trim();
            string sdt = this.txtSDT.Text.Trim();

            khachHang = new ClassKhachHang(makh, tenkh, diachi, sdt);
        }
        private string Query(string textBox)
        {
            if (textBox == "") return "null)";
            else return "'" + textBox + "')";
        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            Enable_2Buttons();
            Enable_Textboxes();
            btnXoa.Enabled = false;
            btnSua.Enabled = false;
        }

        private void btnSua_Click(object sender, EventArgs e)
        {
            Enable_Textboxes();
            Enable_2Buttons();
            btnThem.Enabled = false;
            btnXoa.Enabled = false;
            txtMaKH.ReadOnly = true;
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            Disable_Textboxes();
            Enable_2Buttons();
            btnThem.Enabled = false;
            btnSua.Enabled = false;
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            if (CheckTextBoxes())
            {
                string query = "";
                if (!btnSua.Enabled && !btnXoa.Enabled)
                {
                    getValueTextBoxes();
                    query = "INSERT INTO dbo.KhachHang VALUES";
                    query += "('" + khachHang.Makh + "',";
                    query += "'" + khachHang.Tenkh + "',";
                    query += "'" + khachHang.Diachi + "',";
                    query += Query(khachHang.Sdt);
                    try
                    {
                        if (MessageBox.Show("Bạn có muốn lưu không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
                        {
                            modify.setTable(query);
                            Clear_TextBoxes();
                            Enable_Textboxes();
                            MessageBox.Show("Them vao thanh cong");
                            frmKhachHang_Load(sender, e);
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("lỗi " + ex.Message);
                    }
                }
                if (!btnThem.Enabled && !btnXoa.Enabled)
                {
                    getValueTextBoxes();
                    query = "Update dbo.KhachHang SET ";
                    query += "Tenkh = N'" + txtTenKH.Text + "',";
                    query += "DiaChi = N'" + txtDiaChi.Text + "',";
                    query += "SDT = '" + txtSDT.Text + "'";
                    query += "Where Makh = N'" + txtMaKH.Text + "'";
                    try
                    {
                        if (MessageBox.Show("Bạn có muốn lưu không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
                        {
                            modify.setTable(query);
                            Clear_TextBoxes();
                            Enable_Textboxes();
                            MessageBox.Show("Sửa thành công");
                            frmKhachHang_Load(sender, e);

                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("lỗi " + ex.Message);
                    }
                }
                if (!btnThem.Enabled && !btnSua.Enabled)
                {
                    getValueTextBoxes();
                    query = "Delete From dbo.KhachHang Where Makh =N'" + txtMaKH.Text + "'";
                    try
                    {
                        if (MessageBox.Show("Bạn có muốn lưu không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
                        {
                            modify.setTable(query);
                            MessageBox.Show("Xóa thành công");
                            frmKhachHang_Load(sender, e);
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("lỗi " + ex.Message);
                    }
                }

            }
        }
        private void btnHuy_Click(object sender, EventArgs e)
        {
            frmKhachHang_Load(sender, e);
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                int i = dataGridView1.CurrentRow.Index;
                txtMaKH.Text    = dataGridView1.Rows[i].Cells[0].Value.ToString();
                txtTenKH.Text   = dataGridView1.Rows[i].Cells[1].Value.ToString();
                txtDiaChi.Text  = dataGridView1.Rows[i].Cells[2].Value.ToString();
                txtSDT.Text     = dataGridView1.Rows[i].Cells[3].Value.ToString();

            }
            catch (Exception ex)
            {
                MessageBox.Show("lỗi " + ex.Message);
            }
        }

        private void btnThoat_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnLamMoi_Click(object sender, EventArgs e)
        {
            frmKhachHang_Load(sender, e);
        }

        private void txtSDT_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void txtSDT_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
            }
        }
    }
}
