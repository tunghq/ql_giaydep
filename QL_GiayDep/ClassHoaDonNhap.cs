﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QL_GiayDep
{
    class ClassHoaDonNhap
    {
        private string _mahdn;
        private string _manv;
        private string _ngaynhap;
        private string _mancc;

        public ClassHoaDonNhap()
        {
        }

        public ClassHoaDonNhap(string mahdn, string manv, string ngaynhap, string mancc)
        {
            Mahdn = mahdn;
            Manv = manv;
            Ngaynhap = ngaynhap;
            Mancc = mancc;
        }

        public string Mahdn { get => _mahdn; set => _mahdn = value; }
        public string Manv { get => _manv; set => _manv = value; }
        public string Ngaynhap { get => _ngaynhap; set => _ngaynhap = value; }
        public string Mancc { get => _mancc; set => _mancc = value; }
    }
}
