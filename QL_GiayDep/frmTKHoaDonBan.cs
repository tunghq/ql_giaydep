﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace QL_GiayDep
{
    public partial class frmTKHoaDonBan : Form
    {
        Modify modify;
        public frmTKHoaDonBan()
        {
            InitializeComponent();
        }

        private void frmTKHoaDonBan_Load(object sender, EventArgs e)
        {
            string query = "select *from dbo.HoaDonBan";
            //LOAD DATABASE HOADON
            modify = new Modify();
            try
            {
                dataGridView1.DataSource = modify.Table(query);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Lỗi: " + ex.Message);
            }
            //LOAD Mã NV
            using (SqlConnection sqlConnection = Connection.GetSqlConnection())
            {
                sqlConnection.Open();
                SqlDataAdapter da = new SqlDataAdapter("Select* from dbo.NhanVien where MaCV like 'CV02'", sqlConnection);//SQL là câu truy vấn bảng trong cơ sở dữ liệu, cn là connection đến cơ sở dữ liệu
                DataTable dt = new DataTable();
                da.Fill(dt);
                cbMaNV.DisplayMember = "MaNV";//Word là tên trường bạn muốn hiển thị trong combobox
                cbMaNV.ValueMember = "MaNV";
                cbMaNV.DataSource = dt;
                sqlConnection.Close();
            }
            //LOAD Mã NCC
            using (SqlConnection sqlConnection = Connection.GetSqlConnection())
            {
                sqlConnection.Open();
                SqlDataAdapter da = new SqlDataAdapter("Select* from dbo.KhachHang ", sqlConnection);//SQL là câu truy vấn bảng trong cơ sở dữ liệu, cn là connection đến cơ sở dữ liệu
                DataTable dt = new DataTable();
                da.Fill(dt);
                cbMaKH.DisplayMember = "MaKH";//Word là tên trường bạn muốn hiển thị trong combobox
                cbMaKH.ValueMember = "MaKH";
                cbMaKH.DataSource = dt;
                sqlConnection.Close();
            }
            //LOAD Mã SP
            using (SqlConnection sqlConnection = Connection.GetSqlConnection())
            {
                sqlConnection.Open();
                SqlDataAdapter da = new SqlDataAdapter("Select* from dbo.SanPham ", sqlConnection);//SQL là câu truy vấn bảng trong cơ sở dữ liệu, cn là connection đến cơ sở dữ liệu
                DataTable dt = new DataTable();
                da.Fill(dt);
                cbMaSP.DisplayMember = "MaSP";//Word là tên trường bạn muốn hiển thị trong combobox
                cbMaSP.ValueMember = "MaSP";
                cbMaSP.DataSource = dt;
                sqlConnection.Close();
            }
            Clear_TextBoxes();
        }
        private void Clear_TextBoxes()
        {
            txtMaHDB.Text = "";
            cbMaNV.Text = "";
            cbMaKH.Text = "";
            cbMaSP.Text = "";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                string hd = "";
                if (txtMaHDB.Text.Length > 0 && cbMaNV.Text.Length == 0 && cbMaKH.Text.Length == 0 && cbMaSP.Text.Length == 0)
                {
                    hd = "select * from dbo.HoaDonBan where MaHDB ='" + txtMaHDB.Text + "'";
                }
                if (txtMaHDB.Text.Length == 0 && cbMaNV.Text.Length > 0 && cbMaKH.Text.Length == 0 && cbMaSP.Text.Length == 0)
                {
                    hd = "select * from dbo.HoaDonBan where MaNV ='" + cbMaNV.Text + "'";
                }
                if (txtMaHDB.Text.Length == 0 && cbMaNV.Text.Length == 0 && cbMaKH.Text.Length > 0 && cbMaSP.Text.Length == 0)
                {
                    hd = "select * from dbo.HoaDonBan where MaKH ='" + cbMaKH.Text + "'";
                }
                if (txtMaHDB.Text.Length == 0 && cbMaNV.Text.Length == 0 && cbMaKH.Text.Length == 0 && cbMaSP .Text.Length >0)
                {
                    hd = "select a.MaHDB, a.MaNV, a.NgayBan, a.MaKH, a.TongTien " +
                        "from dbo.HoaDonBan a inner " +
                        "join dbo.ChiTietHDB b " +
                        "on a.MaHDB = b.MaHDB " +
                        "where b.MaSP like N'"+cbMaSP.Text+"'";
                }
                dataGridView1.DataSource = modify.Table(hd);
                if (dataGridView1.RowCount == 0)
                {
                    MessageBox.Show("Dữ liệu bạn tìm không tồn tại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    frmTKHoaDonBan_Load(null, null);
                }
            }catch(Exception ex)
            {
                MessageBox.Show("Lỗi " + ex);
            }
        }

        private void btnLamMoi_Click(object sender, EventArgs e)
        {
            frmTKHoaDonBan_Load(sender, e);
        }
    }
}
