﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace QL_GiayDep
{
    public partial class frmTKSanPham : Form
    {
        Modify modify;

        public frmTKSanPham()
        {
            InitializeComponent();
        }
        private void frmTKSanPham_Load(object sender, EventArgs e)
        {
            //load ma mau
            using (SqlConnection sqlConnection = Connection.GetSqlConnection())
            {
                sqlConnection.Open();
                SqlDataAdapter da = new SqlDataAdapter("Select* from dbo.Mau", sqlConnection);//SQL là câu truy vấn bảng trong cơ sở dữ liệu, cn là connection đến cơ sở dữ liệu
                DataTable dt = new DataTable();
                da.Fill(dt);
                cbMaMau.DisplayMember = "MaMau";//Word là tên trường bạn muốn hiển thị trong combobox
                cbMaMau.ValueMember = "MaMau";
                cbMaMau.DataSource = dt;
                sqlConnection.Close();
            }
            //load ma mua
            using (SqlConnection sqlConnection = Connection.GetSqlConnection())
            {
                sqlConnection.Open();
                SqlDataAdapter da = new SqlDataAdapter("Select* from dbo.Mua", sqlConnection);//SQL là câu truy vấn bảng trong cơ sở dữ liệu, cn là connection đến cơ sở dữ liệu
                DataTable dt = new DataTable();
                da.Fill(dt);
                cbMaMua.DisplayMember = "MaMua";//Word là tên trường bạn muốn hiển thị trong combobox
                cbMaMua.ValueMember = "MaMua";
                cbMaMua.DataSource = dt;
                sqlConnection.Close();
            }
            //load ma Doi Tuong
            using (SqlConnection sqlConnection = Connection.GetSqlConnection())
            {
                sqlConnection.Open();
                SqlDataAdapter da = new SqlDataAdapter("Select* from dbo.DoiTuong", sqlConnection);//SQL là câu truy vấn bảng trong cơ sở dữ liệu, cn là connection đến cơ sở dữ liệu
                DataTable dt = new DataTable();
                da.Fill(dt);
                cbMaDT.DisplayMember = "MaDT";//Word là tên trường bạn muốn hiển thị trong combobox
                cbMaDT.ValueMember = "MaDT";
                cbMaDT.DataSource = dt;
                sqlConnection.Close();
            }
            //load ma chat lieu
            using (SqlConnection sqlConnection = Connection.GetSqlConnection())
            {
                sqlConnection.Open();
                SqlDataAdapter da = new SqlDataAdapter("Select* from dbo.ChatLieu", sqlConnection);//SQL là câu truy vấn bảng trong cơ sở dữ liệu, cn là connection đến cơ sở dữ liệu
                DataTable dt = new DataTable();
                da.Fill(dt);
                cbMaCL.DisplayMember = "MaCL";//Word là tên trường bạn muốn hiển thị trong combobox
                cbMaCL.ValueMember = "MaCL";
                cbMaCL.DataSource = dt;
                sqlConnection.Close();
            }
            //load ma the loai
            using (SqlConnection sqlConnection = Connection.GetSqlConnection())
            {
                sqlConnection.Open();
                SqlDataAdapter da = new SqlDataAdapter("Select* from dbo.TheLoai", sqlConnection);//SQL là câu truy vấn bảng trong cơ sở dữ liệu, cn là connection đến cơ sở dữ liệu
                DataTable dt = new DataTable();
                da.Fill(dt);
                cbMaLoai.DisplayMember = "MaLoai";//Word là tên trường bạn muốn hiển thị trong combobox
                cbMaLoai.ValueMember = "MaLoai";
                cbMaLoai.DataSource = dt;
                sqlConnection.Close();
            }
            //load ma nha san xuat
            using (SqlConnection sqlConnection = Connection.GetSqlConnection())
            {
                sqlConnection.Open();
                SqlDataAdapter da = new SqlDataAdapter("Select* from dbo.NuocSanXuat", sqlConnection);//SQL là câu truy vấn bảng trong cơ sở dữ liệu, cn là connection đến cơ sở dữ liệu
                DataTable dt = new DataTable();
                da.Fill(dt);
                cbMaNSX.DisplayMember = "MaNSX";//Word là tên trường bạn muốn hiển thị trong combobox
                cbMaNSX.ValueMember = "MaNSX";
                cbMaNSX.DataSource = dt;
                sqlConnection.Close();
            }
            //load ma Size
            using (SqlConnection sqlConnection = Connection.GetSqlConnection())
            {
                sqlConnection.Open();
                SqlDataAdapter da = new SqlDataAdapter("Select* from dbo.Size", sqlConnection);//SQL là câu truy vấn bảng trong cơ sở dữ liệu, cn là connection đến cơ sở dữ liệu
                DataTable dt = new DataTable();
                da.Fill(dt);
                cbMaSize.DisplayMember = "MaSize";//Word là tên trường bạn muốn hiển thị trong combobox
                cbMaSize.ValueMember = "MaSize";
                cbMaSize.DataSource = dt;
                sqlConnection.Close();
            }

            string query = "Select* from dbo.SanPham";
            modify = new Modify();
            try
            {
                dataGridView1.DataSource = modify.Table(query);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Lỗi: " + ex.Message);
            }
            Clear_TextBoxes();
        }
        private void Clear_TextBoxes()
        {
            txtMaSP.Text = "";
            txtTenSP.Text = "";
            cbMaMau.Text = "";
            cbMaMua.Text = "";
            cbMaDT.Text = "";
            cbMaLoai.Text = "";
            cbMaNSX.Text = "";
            cbMaSize.Text = "";
            cbMaCL.Text = "";

        }
        private void button1_Click(object sender, EventArgs e)
        {
            string sql = "";
            if (txtMaSP.Text.Length > 0 && txtTenSP.Text.Length == 0 && cbMaCL.Text.Length == 0 && cbMaDT.Text.Length == 0
                && cbMaLoai.Text.Length == 0 && cbMaMau.Text.Length == 0 && cbMaMua.Text.Length == 0 && cbMaNSX.Text.Length == 0
                && cbMaSize.Text.Length == 0)
            {
                sql = "exec dbo.TimKiemMa N'"+txtMaSP.Text+"'";
            }
            if (txtMaSP.Text.Length == 0 && txtTenSP.Text.Length > 0 && cbMaCL.Text.Length == 0 && cbMaDT.Text.Length == 0
               && cbMaLoai.Text.Length == 0 && cbMaMau.Text.Length == 0 && cbMaMua.Text.Length == 0 && cbMaNSX.Text.Length == 0
               && cbMaSize.Text.Length == 0)
            {
                sql = "exec dbo.TimKiem N'"+txtTenSP.Text+"'";
            }
            if (txtMaSP.Text.Length == 0 && txtTenSP.Text.Length == 0 && cbMaCL.Text.Length > 0 && cbMaDT.Text.Length == 0
               && cbMaLoai.Text.Length == 0 && cbMaMau.Text.Length == 0 && cbMaMua.Text.Length == 0 && cbMaNSX.Text.Length == 0
               && cbMaSize.Text.Length == 0)
            {
                sql = "select * from dbo.SanPham where MaCL ='" + cbMaCL.Text + "'";
            }
            if (txtMaSP.Text.Length == 0 && txtTenSP.Text.Length == 0 && cbMaCL.Text.Length == 0 && cbMaDT.Text.Length > 0
               && cbMaLoai.Text.Length == 0 && cbMaMau.Text.Length == 0 && cbMaMua.Text.Length == 0 && cbMaNSX.Text.Length == 0
               && cbMaSize.Text.Length == 0)
            {
                sql = "select * from dbo.SanPham where MaDT ='" + cbMaDT.Text + "'";
            }
            if (txtMaSP.Text.Length == 0 && txtTenSP.Text.Length == 0 && cbMaCL.Text.Length == 0 && cbMaDT.Text.Length == 0
               && cbMaLoai.Text.Length > 0 && cbMaMau.Text.Length == 0 && cbMaMua.Text.Length == 0 && cbMaNSX.Text.Length == 0
               && cbMaSize.Text.Length == 0)
            {
                sql = "select * from dbo.SanPham where MaLoai ='" + cbMaLoai.Text + "'";
            }
            if (txtMaSP.Text.Length == 0 && txtTenSP.Text.Length == 0 && cbMaCL.Text.Length == 0 && cbMaDT.Text.Length == 0
               && cbMaLoai.Text.Length == 0 && cbMaMau.Text.Length > 0 && cbMaMua.Text.Length == 0 && cbMaNSX.Text.Length == 0
               && cbMaSize.Text.Length == 0)
            {
                sql = "select * from dbo.SanPham where MaMau ='" + cbMaMau.Text + "'";
            }
            if (txtMaSP.Text.Length == 0 && txtTenSP.Text.Length == 0 && cbMaCL.Text.Length == 0 && cbMaDT.Text.Length == 0
               && cbMaLoai.Text.Length == 0 && cbMaMau.Text.Length == 0 && cbMaMua.Text.Length > 0 && cbMaNSX.Text.Length == 0
               && cbMaSize.Text.Length == 0)
            {
                sql = "select * from dbo.SanPham where MaMua ='" + cbMaMua.Text + "'";
            }
            if (txtMaSP.Text.Length == 0 && txtTenSP.Text.Length == 0 && cbMaCL.Text.Length == 0 && cbMaDT.Text.Length == 0
               && cbMaLoai.Text.Length == 0 && cbMaMau.Text.Length == 0 && cbMaMua.Text.Length == 0 && cbMaNSX.Text.Length > 0
               && cbMaSize.Text.Length == 0)
            {
                sql = "select * from dbo.SanPham where MaNSX ='" + cbMaNSX.Text + "'";
            }
            if (txtMaSP.Text.Length == 0 && txtTenSP.Text.Length == 0 && cbMaCL.Text.Length == 0 && cbMaDT.Text.Length == 0
               && cbMaLoai.Text.Length == 0 && cbMaMau.Text.Length == 0 && cbMaMua.Text.Length == 0 && cbMaNSX.Text.Length == 0
               && cbMaSize.Text.Length > 0)
            {
                sql = "select * from dbo.SanPham where MaSize ='" + cbMaSize.Text + "'";
            }
            dataGridView1.DataSource = modify.Table(sql);
            if(dataGridView1.RowCount == 0)
            {
                MessageBox.Show("Dữ liệu bạn tìm không tồn tại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                frmTKSanPham_Load(null, null);              
            }
        }

        private void btnLamMoi_Click(object sender, EventArgs e)
        {
            frmTKSanPham_Load(sender,e);
        }
    }
}
