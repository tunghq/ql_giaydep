﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QL_GiayDep
{
    class ClassSanPham
    {
        private string __masp;
        private string __tensp;
        private string __maloai;
        private string __masize;
        private string __macl;
        private string __mamau;
        private string __madt;
        private string __mamua;
        private string __mansx;
        private string __hinhanh;
        private double __gianhap;
        private double __giaban;

        public ClassSanPham()
        {
        }

        public ClassSanPham(string masp, string tensp, string maloai, string masize, string macl, string mamau, string madt, string mamua, string mansx, string hinhanh, double gianhap, double giaban)
        {
            Masp = masp;
            Tensp = tensp;
            Maloai = maloai;
            Masize = masize;
            Macl = macl;
            Mamau = mamau;
            Madt = madt;
            Mamua = mamua;
            Mansx = mansx;
            Hinhanh = hinhanh;
            Gianhap = gianhap;
            Giaban = giaban;
        }

        public string Masp { get => __masp; set => __masp = value; }
        public string Tensp { get => __tensp; set => __tensp = value; }
        public string Maloai { get => __maloai; set => __maloai = value; }
        public string Masize { get => __masize; set => __masize = value; }
        public string Macl { get => __macl; set => __macl = value; }
        public string Mamau { get => __mamau; set => __mamau = value; }
        public string Madt { get => __madt; set => __madt = value; }
        public string Mamua { get => __mamua; set => __mamua = value; }
        public string Mansx { get => __mansx; set => __mansx = value; }
        public string Hinhanh { get => __hinhanh; set => __hinhanh = value; }
        public double Gianhap { get => __gianhap; set => __gianhap = value; }
        public double Giaban { get => __giaban; set => __giaban = value; }
    }
}
