﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace QL_GiayDep
{
    public partial class frmChiTietHDN : Form
    {
        Modify modify;
        ClassChiTietHDN chiTietHDN;
        public frmChiTietHDN()
        {
            InitializeComponent();
        }

        private void frmChiTietHDN_Load(object sender, EventArgs e)
        {
            //LOAD Mã SP
            using (SqlConnection sqlConnection = Connection.GetSqlConnection())
            {
                sqlConnection.Open();
                SqlDataAdapter da = new SqlDataAdapter("Select* from dbo.SanPham ", sqlConnection);//SQL là câu truy vấn bảng trong cơ sở dữ liệu, cn là connection đến cơ sở dữ liệu
                DataTable dt = new DataTable();
                da.Fill(dt);
                cbMaSP.DisplayMember = "MaSP";//Word là tên trường bạn muốn hiển thị trong combobox
                cbMaSP.ValueMember = "MaSP";
                cbMaSP.DataSource = dt;
                sqlConnection.Close();
            }
            //LOAD MAHDN
            using (SqlConnection sqlConnection = Connection.GetSqlConnection())
            {
                sqlConnection.Open();
                SqlDataAdapter da = new SqlDataAdapter("Select* from dbo.HoaDonNhap ", sqlConnection);//SQL là câu truy vấn bảng trong cơ sở dữ liệu, cn là connection đến cơ sở dữ liệu
                DataTable dt = new DataTable();
                da.Fill(dt);
                cbMaHDN.DisplayMember = "MaHDN";//Word là tên trường bạn muốn hiển thị trong combobox
                cbMaHDN.ValueMember = "MaHDN";
                cbMaHDN.DataSource = dt;
                sqlConnection.Close();
            }
            Clear_TextBoxes();
            Disable_Textboxes();
            Disable_2Buttons();
            Enable_3Buttons();
        }
        private void Disable_Textboxes()
        {
            cbMaSP.Enabled = false;
            txtGiamGia.ReadOnly = true;
            txtSoLuong.ReadOnly = true;
        }
        private void Enable_Textboxes()
        {
            cbMaSP.Enabled = true;
            txtGiamGia.ReadOnly = false;
            txtSoLuong.ReadOnly = false;
        }
        private void Enable_2Buttons()
        {
            btnHuy.Enabled = true;
            btnLuu.Enabled = true;
        }
        private void Disable_2Buttons()
        {
            btnHuy.Enabled = false;
            btnLuu.Enabled = false;
        }
        private void Enable_3Buttons()
        {
            btnThem.Enabled = true;
            btnXoa.Enabled = true;
        }
        private void Clear_TextBoxes()
        {
            cbMaSP.Text = "";
            txtGiamGia.Text = "";
            txtSoLuong.Text = "";
        }
        private bool CheckTextBoxesCTHDN()
        {
            if (txtSoLuong.Text.Trim() == "")//su dung trim khong lay khoang trang giua cac ki tu
            {
                MessageBox.Show("Vui lòng nhập số lượng mua");
                return false;
            }
            if (txtGiamGia.Text.Trim() == "")//su dung trim khong lay khoang trang giua cac ki tu
            {
                MessageBox.Show("Vui long nhapkhuyến mãi của mặt hàng nếu không có thì điền 0");
                return false;
            }
            return true;
        }
        private void getValueCTHDN()
        {
            string mahdn = this.cbMaHDN.Text.Trim();
            string masp = this.cbMaSP.Text.Trim();
            int soluong = Convert.ToInt32(txtSoLuong.Text.Trim());
            double giamgia = Convert.ToDouble(txtGiamGia.Text.Trim());
            chiTietHDN = new ClassChiTietHDN(mahdn, masp, soluong, giamgia);
        }

        private void cbMaHDN_SelectedIndexChanged(object sender, EventArgs e)
        {
            string query = "select *from dbo.ChiTietHDN where MaHDN = N'"+cbMaHDN.Text+"'";
            modify = new Modify();
            try
            {
                dataGridView1.DataSource = modify.Table(query);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Lỗi: " + ex.Message);
            }
        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            Enable_2Buttons();
            Enable_Textboxes();
            btnXoa.Enabled = false;
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            Disable_Textboxes();
            Enable_2Buttons();
            btnThem.Enabled = false;
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            if (CheckTextBoxesCTHDN())
            {
                string query = "";
                if (!btnXoa.Enabled)
                {
                    getValueCTHDN();
                    query = "INSERT [dbo].[ChiTietHDN]([MaHDN], [MaSP], [SLNhap],[GiamGia],[ThanhTien]) VALUES (N'" + chiTietHDN.Mahdn + "', N'" + chiTietHDN.Masp + "'," + chiTietHDN.Soluong + "," + chiTietHDN.Giamgia + ",NULL)";
                    try
                    {
                        if (MessageBox.Show("Bạn có muốn lưu không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
                        {
                            modify.setTable(query);
                            Clear_TextBoxes();
                            Enable_Textboxes();
                            MessageBox.Show("Thêm vào thành công");
                            frmChiTietHDN_Load(sender, e);
                            string sql = "select *from dbo.ChiTietHDN where MaHDN = N'" + cbMaHDN.Text + "'";
                            dataGridView1.DataSource = modify.Table(sql);
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("lỗi " + ex.Message);
                    }
                }

                if (!btnThem.Enabled)
                {
                    getValueCTHDN();
                    query = "Delete From dbo.ChitietHDN Where MaSP  like N'" + cbMaSP.Text + "'AND MaHDN like N'" + cbMaHDN.Text + "'";
                    try
                    {
                        if (MessageBox.Show("Bạn có muốn lưu không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
                        {
                            modify.setTable(query);
                            MessageBox.Show("Xóa thành công");
                            string sql = "select *from dbo.ChiTietHDN where MaHDN = N'" + cbMaHDN.Text + "'";
                            dataGridView1.DataSource = modify.Table(sql);
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("lỗi " + ex.Message);
                    }
                }
            }
        }
        private void btnHuy_Click(object sender, EventArgs e)
        {
            frmChiTietHDN_Load(sender, e);
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                int i = dataGridView1.CurrentRow.Index;
                cbMaHDN.Text = dataGridView1.Rows[i].Cells[0].Value.ToString();
                cbMaSP.Text     = dataGridView1.Rows[i].Cells[1].Value.ToString();
                txtSoLuong.Text = dataGridView1.Rows[i].Cells[2].Value.ToString();
                txtGiamGia.Text = dataGridView1.Rows[i].Cells[3].Value.ToString();

            }
            catch (Exception ex)
            {
                MessageBox.Show("lỗi " + ex.Message);
            }
        }

        private void btnThoat_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void txtSoLuong_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
            }
        }

        private void txtGiamGia_KeyPress(object sender, KeyPressEventArgs e)
        {
            // Xác thực rằng phím vừa nhấn không phải CTRL hoặc không phải dạng số
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
            }

            // Nếu bạn muốn, bạn có thể cho phép nhập số thực với dấu chấm
            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }

        private void txtSoLuong_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
