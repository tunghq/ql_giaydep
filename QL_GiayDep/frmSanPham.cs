﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Data.SqlClient;

namespace QL_GiayDep
{
    public partial class frmSanPham : Form
    {
        Modify modify;
        ClassSanPham sanPham;
        public frmSanPham()
        {
            InitializeComponent();
        }

        private void frmSanPham_Load(object sender, EventArgs e)
        {
            //load ma mau
            using (SqlConnection sqlConnection = Connection.GetSqlConnection())
            {
                sqlConnection.Open();
                SqlDataAdapter da = new SqlDataAdapter("Select* from dbo.Mau", sqlConnection);//SQL là câu truy vấn bảng trong cơ sở dữ liệu, cn là connection đến cơ sở dữ liệu
                DataTable dt = new DataTable();
                da.Fill(dt);
                cbMaMau.DisplayMember = "MaMau";//Word là tên trường bạn muốn hiển thị trong combobox
                cbMaMau.ValueMember = "MaMau";
                cbMaMau.DataSource = dt;
                sqlConnection.Close();
            }
            //load ma mua
            using (SqlConnection sqlConnection = Connection.GetSqlConnection())
            {
                sqlConnection.Open();
                SqlDataAdapter da = new SqlDataAdapter("Select* from dbo.Mua", sqlConnection);//SQL là câu truy vấn bảng trong cơ sở dữ liệu, cn là connection đến cơ sở dữ liệu
                DataTable dt = new DataTable();
                da.Fill(dt);
                cbMaMua.DisplayMember = "MaMua";//Word là tên trường bạn muốn hiển thị trong combobox
                cbMaMua.ValueMember = "MaMua";
                cbMaMua.DataSource = dt;
                sqlConnection.Close();
            }
            //load ma Doi Tuong
            using (SqlConnection sqlConnection = Connection.GetSqlConnection())
            {
                sqlConnection.Open();
                SqlDataAdapter da = new SqlDataAdapter("Select* from dbo.DoiTuong", sqlConnection);//SQL là câu truy vấn bảng trong cơ sở dữ liệu, cn là connection đến cơ sở dữ liệu
                DataTable dt = new DataTable();
                da.Fill(dt);
                cbMaDT.DisplayMember = "MaDT";//Word là tên trường bạn muốn hiển thị trong combobox
                cbMaDT.ValueMember = "MaDT";
                cbMaDT.DataSource = dt;
                sqlConnection.Close();
            }
            //load ma chat lieu
            using (SqlConnection sqlConnection = Connection.GetSqlConnection())
            {
                sqlConnection.Open();
                SqlDataAdapter da = new SqlDataAdapter("Select* from dbo.ChatLieu", sqlConnection);//SQL là câu truy vấn bảng trong cơ sở dữ liệu, cn là connection đến cơ sở dữ liệu
                DataTable dt = new DataTable();
                da.Fill(dt);
                cbMaCL.DisplayMember = "MaCL";//Word là tên trường bạn muốn hiển thị trong combobox
                cbMaCL.ValueMember = "MaCL";
                cbMaCL.DataSource = dt;
                sqlConnection.Close();
            }
            //load ma the loai
            using (SqlConnection sqlConnection = Connection.GetSqlConnection())
            {
                sqlConnection.Open();
                SqlDataAdapter da = new SqlDataAdapter("Select* from dbo.TheLoai", sqlConnection);//SQL là câu truy vấn bảng trong cơ sở dữ liệu, cn là connection đến cơ sở dữ liệu
                DataTable dt = new DataTable();
                da.Fill(dt);
                cbMaLoai.DisplayMember = "MaLoai";//Word là tên trường bạn muốn hiển thị trong combobox
                cbMaLoai.ValueMember = "MaLoai";
                cbMaLoai.DataSource = dt;
                sqlConnection.Close();
            }
            //load ma nha san xuat
            using (SqlConnection sqlConnection = Connection.GetSqlConnection())
            {
                sqlConnection.Open();
                SqlDataAdapter da = new SqlDataAdapter("Select* from dbo.NuocSanXuat", sqlConnection);//SQL là câu truy vấn bảng trong cơ sở dữ liệu, cn là connection đến cơ sở dữ liệu
                DataTable dt = new DataTable();
                da.Fill(dt);
                cbMaNSX.DisplayMember = "MaNSX";//Word là tên trường bạn muốn hiển thị trong combobox
                cbMaNSX.ValueMember = "MaNSX";
                cbMaNSX.DataSource = dt;
                sqlConnection.Close();
            }
            //load ma Size
            using (SqlConnection sqlConnection = Connection.GetSqlConnection())
            {
                sqlConnection.Open();
                SqlDataAdapter da = new SqlDataAdapter("Select* from dbo.Size", sqlConnection);//SQL là câu truy vấn bảng trong cơ sở dữ liệu, cn là connection đến cơ sở dữ liệu
                DataTable dt = new DataTable();
                da.Fill(dt);
                cbMaSize.DisplayMember = "MaSize";//Word là tên trường bạn muốn hiển thị trong combobox
                cbMaSize.ValueMember = "MaSize";
                cbMaSize.DataSource = dt;
                sqlConnection.Close();
            }
            
            string query = "Select* from dbo.SanPham";
            modify = new Modify();
            try
            {
                dataGridView1.DataSource = modify.Table(query);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Lỗi: " + ex.Message);
            }
            Clear_TextBoxes();
            Disable_Textboxes();
            Disable_2Buttons();
            Enable_3Buttons();
            InVisible_Gia();
        }

        private void btnChonAnh_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = openFileDialog.Filter = "JPG File (*.jpg)|*.jpg|ALL File (*.*)|*.*";
            openFileDialog.FilterIndex = 1;
            openFileDialog.RestoreDirectory = true;
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                pictureBox1.ImageLocation = openFileDialog.FileName;
                txtHinhAnh.Text = openFileDialog.FileName;
            }
        }

        private void txtMaSP_TextChanged(object sender, EventArgs e)
        {
            double r = 0;
            Random rand = new Random();
            r = rand.Next(100000, 1000000);
            txtGiaNhap.Text = r.ToString();
            txtGiaBan.Text = (r + r * 0.1).ToString();
        }
        private void Disable_Textboxes()
        {
            txtMaSP.ReadOnly = true;
            txtTenSP.ReadOnly = true;
            cbMaCL.Enabled = false;
            cbMaDT.Enabled = false;
            cbMaLoai.Enabled = false;
            cbMaMau.Enabled = false;
            cbMaMua.Enabled = false;
            cbMaNSX.Enabled = false;
            cbMaSize.Enabled = false;

        }
        private void Enable_Textboxes()
        {
            txtMaSP.ReadOnly = false;
            txtTenSP.ReadOnly = false;
            cbMaCL.Enabled = true;
            cbMaDT.Enabled = true;
            cbMaLoai.Enabled = true;
            cbMaMau.Enabled = true;
            cbMaMua.Enabled = true;
            cbMaNSX.Enabled = true;
            cbMaSize.Enabled = true;


        }
        private void Visible_Gia()
        {
            label12.Visible = true;
            label13.Visible = true;
            txtGiaBan.Visible = true;
            txtGiaNhap.Visible = true;
        }
        private void InVisible_Gia()
        {
            label12.Visible = false;
            label13.Visible = false;
            txtGiaBan.Visible = false;
            txtGiaNhap.Visible = false;
        }
        private void Enable_2Buttons()
        {
            btnHuy.Enabled = true;
            btnLuu.Enabled = true;
        }
        private void Disable_2Buttons()
        {
            btnHuy.Enabled = false;
            btnLuu.Enabled = false;
        }
        private void Enable_3Buttons()
        {
            btnThem.Enabled = true;
            btnSua.Enabled = true;
            btnXoa.Enabled = true;
        }
        private void Clear_TextBoxes()
        {
            txtMaSP.Text = "";
            txtTenSP.Text = "";
            txtHinhAnh.Text = "";
            txtGiaBan.Text = "";
            txtGiaNhap.Text = "";
            pictureBox1.Image = null;
            cbMaMau.Text = "";
            cbMaMua.Text = "";
            cbMaDT.Text = "";
            cbMaLoai.Text = "";
            cbMaNSX.Text = "";
            cbMaSize.Text = "";
            cbMaCL.Text = "";

        }
        private bool CheckTextBoxes()
        {
            if (txtMaSP.Text.Trim() == "")//su dung trim khong lay khoang trang giua cac ki tu
            {
                MessageBox.Show("Vui lòng nhập mã sản phẩm");
                return false;
            }
            if (txtTenSP.Text.Trim() == "")//su dung trim khong lay khoang trang giua cac ki tu
            {
                MessageBox.Show("Vui lòng nhập tên sản phẩm");
                return false;
            }
            return true;
            
        }
        private void getValueTextBoxes()
        {

            string masp = this.txtMaSP.Text.Trim();
            string tensp = this.txtTenSP.Text.Trim();
            string maloai = this.cbMaLoai.Text.Trim();
            string masize = this.cbMaSize.Text.Trim();
            string macl = this.cbMaCL.Text.Trim();
            string mamau = this.cbMaMau.Text.Trim();
            string madt = this.cbMaDT.Text.Trim();
            string mamua = this.cbMaMua.Text.Trim();
            string mansx = this.cbMaNSX.Text.Trim();
            string hinhanh = this.txtHinhAnh.Text.Trim();
            double gianhap = Convert.ToDouble(this.txtGiaNhap.Text.Trim());
            double giaban = Convert.ToDouble(this.txtGiaBan.Text.Trim());

            sanPham = new ClassSanPham(masp, tensp, maloai, masize, macl, mamau, madt, mamua, mansx, hinhanh, gianhap, giaban);
        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            InVisible_Gia();
            Enable_2Buttons();
            Enable_Textboxes();
            btnXoa.Enabled = false;
            btnSua.Enabled = false;
        }

        private void btnSua_Click(object sender, EventArgs e)
        {
            InVisible_Gia();
            Enable_Textboxes();
            Enable_2Buttons();
            btnThem.Enabled = false;
            btnXoa.Enabled = false;
            txtMaSP.ReadOnly = true;
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            InVisible_Gia();
            Disable_Textboxes();
            Enable_2Buttons();
            btnThem.Enabled = false;
            btnSua.Enabled = false;
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            if (CheckTextBoxes())
            {
                string query = "";
                if (!btnSua.Enabled && !btnXoa.Enabled)
                {
                    getValueTextBoxes();
                    query = " INSERT[dbo].[SanPham]([MaSP], [TenSP],[MaLoai], [MaSize], [MaCL], [MaMau],[MaDT], [MaMua], [MaNSX],[HinhAnh],[GiaNhap],[GiaBan],[SoLuongNhap],[SoLuongBan],[SoLuongTon]) VALUES(N'" + sanPham.Masp + "', N'" + sanPham.Tensp + "', N'" + sanPham.Maloai + "', N'" + sanPham.Masize + "', N'" + sanPham.Macl + "', N'" + sanPham.Mamau + "', N'" + sanPham.Madt + "', N'" + sanPham.Mamua + "', N'" + sanPham.Mansx + "', NULL, " + sanPham.Gianhap + ", " + sanPham.Giaban + ", NULL, NULL, NULL) UPDATE SanPham SET[HinhAnh] = (SELECT MyImage.* from Openrowset(Bulk '" + sanPham.Hinhanh + "', Single_Blob) MyImage)where MaSP = N'" + sanPham.Masp + "'";
                    try
                    {
                        if (MessageBox.Show("Bạn có muốn lưu không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
                        {
                            modify.setTable(query);
                            Clear_TextBoxes();
                            Enable_Textboxes();
                            MessageBox.Show("Thêm thành công");
                            frmSanPham_Load(sender, e);
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("lỗi " + ex.Message);
                    }
                }
                if (!btnThem.Enabled && !btnXoa.Enabled)
                {
                    getValueTextBoxes();
                    if (txtHinhAnh.Text == "")
                    {
                        query = "Update dbo.SanPham SET ";
                        query += "TenSP = N'" + txtTenSP.Text + "',";
                        query += "MaLoai = N'" + cbMaLoai.Text + "',";
                        query += "MaSize = N'" + cbMaSize.Text + "',";
                        query += "MaCL = N'" + cbMaCL.Text + "',";
                        query += "MaMau = N'" + cbMaMau.Text + "',";
                        query += "MaDT = N'" + cbMaDT.Text + "',";
                        query += "MaMua = N'" + cbMaMua.Text + "',";
                        query += "MaNSX = N'" + cbMaNSX.Text + "',";
                        //query += "HinhAnh = N'" +    txtHinhAnh.Text    + "',";
                        query += "GiaNhap = '" + txtGiaNhap.Text + "',";
                        query += "GiaBan = '" + txtGiaBan.Text + "'";
                        query += "Where MaSP = N'" + txtMaSP.Text + "'";
                        try
                        {
                            if (MessageBox.Show("Bạn có muốn lưu không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
                            {
                                modify.setTable(query);
                                Clear_TextBoxes();
                                Enable_Textboxes();
                                MessageBox.Show("Sửa thành công");
                                frmSanPham_Load(sender, e);

                            }
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("lỗi " + ex.Message);
                        }
                    }
                    else
                    {
                        query = "Update dbo.SanPham SET ";
                        query += "TenSP = N'" + txtTenSP.Text + "',";
                        query += "MaLoai = N'" + cbMaLoai.Text + "',";
                        query += "MaSize = N'" + cbMaSize.Text + "',";
                        query += "MaCL = N'" + cbMaCL.Text + "',";
                        query += "MaMau = N'" + cbMaMau.Text + "',";
                        query += "MaDT = N'" + cbMaDT.Text + "',";
                        query += "MaMua = N'" + cbMaMua.Text + "',";
                        query += "MaNSX = N'" + cbMaNSX.Text + "',";
                        query += "GiaNhap = '" + txtGiaNhap.Text + "',";
                        query += "GiaBan = '" + txtGiaBan.Text + "'";
                        query += "Where MaSP = N'" + txtMaSP.Text + "'";
                        query += "UPDATE SanPham SET[HinhAnh] = (SELECT MyImage.* from Openrowset(Bulk '" + txtHinhAnh.Text + "', Single_Blob) MyImage)where MaSP = N'" + txtMaSP.Text + "'"; 
                        try
                        {
                            if (MessageBox.Show("Bạn có muốn lưu không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
                            {
                                modify.setTable(query);
                                Clear_TextBoxes();
                                Enable_Textboxes();
                                MessageBox.Show("Sửa thành công");
                                frmSanPham_Load(sender, e);

                            }
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("lỗi " + ex.Message);
                        }
                    }
                }
                if (!btnThem.Enabled && !btnSua.Enabled)
                {
                    getValueTextBoxes();
                    query = "Delete From dbo.SanPham Where MaSP =N'" + txtMaSP.Text + "'";
                    try
                    {
                        if (MessageBox.Show("Bạn có muốn lưu không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
                        {
                            modify.setTable(query);
                            MessageBox.Show("Xóa thành công");
                            frmSanPham_Load(sender, e);
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("lỗi " + ex.Message);
                    }
                }

            }
        }

        private void btnHuy_Click(object sender, EventArgs e)
        {
            frmSanPham_Load(sender, e);
        }

        private void btnThoat_Click(object sender, EventArgs e)
        {
            Close();
        }



        private void dataGridView1_CellContentClick_1(object sender, DataGridViewCellEventArgs e)
        {
            Visible_Gia();

            int i = dataGridView1.CurrentRow.Index;
            txtMaSP.Text    = dataGridView1.Rows[i].Cells[0].Value.ToString();
            txtTenSP.Text   = dataGridView1.Rows[i].Cells[1].Value.ToString();
            cbMaLoai.Text   = dataGridView1.Rows[i].Cells[2].Value.ToString();
            cbMaSize.Text   = dataGridView1.Rows[i].Cells[3].Value.ToString();
            cbMaCL.Text     = dataGridView1.Rows[i].Cells[4].Value.ToString();
            cbMaMau.Text    = dataGridView1.Rows[i].Cells[5].Value.ToString();
            cbMaDT.Text     = dataGridView1.Rows[i].Cells[6].Value.ToString();
            cbMaMua.Text    = dataGridView1.Rows[i].Cells[7].Value.ToString();
            cbMaNSX.Text    = dataGridView1.Rows[i].Cells[8].Value.ToString();
            byte[] imgData  = (byte[])dataGridView1.Rows[i].Cells[12].Value;
            txtGiaNhap.Text = dataGridView1.Rows[i].Cells[13].Value.ToString();
            txtGiaBan.Text  = dataGridView1.Rows[i].Cells[14].Value.ToString();
            MemoryStream ms = new MemoryStream(imgData);
            pictureBox1.Image = Image.FromStream(ms);

        }

        private void btnLamMoi_Click(object sender, EventArgs e)
        {
            frmSanPham_Load(sender, e);
        }

        private void txtGiaBan_TextChanged(object sender, EventArgs e)
        {

        }

        private void cbMaLoai_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
