﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace QL_GiayDep
{
    public partial class frmTKHoaDonNhap : Form
    {
        Modify modify;
        public frmTKHoaDonNhap()
        {
            InitializeComponent();
        }
        private void frmTKHoaDonNhap_Load(object sender, EventArgs e)
        {
            //LOAD Mã SP
            using (SqlConnection sqlConnection = Connection.GetSqlConnection())
            {
                sqlConnection.Open();
                SqlDataAdapter da = new SqlDataAdapter("Select* from dbo.SanPham ", sqlConnection);//SQL là câu truy vấn bảng trong cơ sở dữ liệu, cn là connection đến cơ sở dữ liệu
                DataTable dt = new DataTable();
                da.Fill(dt);
                cbMaSP.DisplayMember = "MaSP";//Word là tên trường bạn muốn hiển thị trong combobox
                cbMaSP.ValueMember = "MaSP";
                cbMaSP.DataSource = dt;
                sqlConnection.Close();
            }
            string query = "select *from dbo.HoaDonNhap";
            //LOAD DATABASE HOADON
            modify = new Modify();
            try
            {
                dataGridView1.DataSource = modify.Table(query);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Lỗi: " + ex.Message);
            }
            //LOAD Mã NV
            using (SqlConnection sqlConnection = Connection.GetSqlConnection())
            {
                sqlConnection.Open();
                SqlDataAdapter da = new SqlDataAdapter("Select* from dbo.NhanVien where MaCV like 'CV01'", sqlConnection);//SQL là câu truy vấn bảng trong cơ sở dữ liệu, cn là connection đến cơ sở dữ liệu
                DataTable dt = new DataTable();
                da.Fill(dt);
                cbMaNV.DisplayMember = "MaNV";//Word là tên trường bạn muốn hiển thị trong combobox
                cbMaNV.ValueMember = "MaNV";
                cbMaNV.DataSource = dt;
                sqlConnection.Close();
            }
            //LOAD Mã NCC
            using (SqlConnection sqlConnection = Connection.GetSqlConnection())
            {
                sqlConnection.Open();
                SqlDataAdapter da = new SqlDataAdapter("Select* from dbo.NhaCungCap ", sqlConnection);//SQL là câu truy vấn bảng trong cơ sở dữ liệu, cn là connection đến cơ sở dữ liệu
                DataTable dt = new DataTable();
                da.Fill(dt);
                cbMaNCC.DisplayMember = "MaNCC";//Word là tên trường bạn muốn hiển thị trong combobox
                cbMaNCC.ValueMember = "MaNCC";
                cbMaNCC.DataSource = dt;
                sqlConnection.Close();
            }
            Clear_TextBoxes();
        }
        private void Clear_TextBoxes()
        {
            cbMaSP.Text = "";
            txtMaHDN.Text = "";
            cbMaNV.Text = "";
            cbMaNCC.Text = "";
        }
        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                string hd = "";
                if (txtMaHDN.Text.Length > 0 && cbMaNV.Text.Length == 0 && cbMaNCC.Text.Length == 0 && cbMaSP.Text.Length == 0)
                {
                    hd = "select * from dbo.HoaDonNhap where MaHDN ='" + txtMaHDN.Text + "'";
                }
                if (txtMaHDN.Text.Length == 0 && cbMaNV.Text.Length > 0 && cbMaNCC.Text.Length == 0 && cbMaSP.Text.Length == 0)
                {
                    hd = "select * from dbo.HoaDonNhap where MaNV ='" + cbMaNV.Text + "'";
                }
                if (txtMaHDN.Text.Length == 0 && cbMaNV.Text.Length == 0 && cbMaNCC.Text.Length > 0 && cbMaSP.Text.Length == 0)
                {
                    hd = "select * from dbo.HoaDonNhap where MaNCC ='" + cbMaNCC.Text + "'";
                }
                if (txtMaHDN.Text.Length == 0 && cbMaNV.Text.Length == 0 && cbMaNCC.Text.Length == 0 && cbMaSP.Text.Length > 0)
                {
                    hd = "select a.MaHDN, a.MaNV, a.NgayNhap, a.MaNCC, a.TongTien " +
                        "from dbo.HoaDonNhap a inner join dbo.ChiTietHDN b " +
                        "on a.MaHDN = b.MaHDN " +
                        "where b.MaSP like N'"+cbMaSP.Text+"'";
                }
                dataGridView1.DataSource = modify.Table(hd);
                if (dataGridView1.RowCount == 0)
                {
                    MessageBox.Show("Dữ liệu bạn tìm không tồn tại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    frmTKHoaDonNhap_Load(null, null);
                }
            }catch(Exception ex)
            {
                MessageBox.Show("Lỗi" + ex);
            }
        }

        private void btnLamMoi_Click(object sender, EventArgs e)
        {
            frmTKHoaDonNhap_Load(sender, e);
        }
    }
}
