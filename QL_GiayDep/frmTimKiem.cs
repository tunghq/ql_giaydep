﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QL_GiayDep
{
    public partial class frmTimKiem : Form
    {
        public frmTimKiem()
        {
            InitializeComponent();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(comboBox1.SelectedIndex == 0)
            {
                frmTKSanPham f = new frmTKSanPham();
                f.MdiParent = this;
                f.Show();
            }
            if (comboBox1.SelectedIndex == 1)
            {
                frmTKHoaDonNhap f = new frmTKHoaDonNhap();
                f.MdiParent = this;
                f.Show();
            }
            if (comboBox1.SelectedIndex == 2)
            {
                frmTKHoaDonBan f = new frmTKHoaDonBan();
                f.MdiParent = this;
                f.Show();
            }
        }

        private void btnThoat_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
