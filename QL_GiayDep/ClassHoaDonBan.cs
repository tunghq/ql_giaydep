﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QL_GiayDep
{
    class ClassHoaDonBan
    {
        private string _mahdb;
        private string _manv;
        private string _ngayban;
        private string _mankh;

        public ClassHoaDonBan()
        {
        }

        public ClassHoaDonBan(string mahdb, string manv, string ngayban, string mankh)
        {
            Mahdb = mahdb;
            Manv = manv;
            Ngayban = ngayban;
            Mankh = mankh;
        }

        public string Mahdb { get => _mahdb; set => _mahdb = value; }
        public string Manv { get => _manv; set => _manv = value; }
        public string Ngayban { get => _ngayban; set => _ngayban = value; }
        public string Mankh { get => _mankh; set => _mankh = value; }
    }
}
