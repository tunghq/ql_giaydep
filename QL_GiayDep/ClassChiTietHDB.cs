﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QL_GiayDep
{
    class ClassChiTietHDB
    {
        private string __mahdb;
        private string __masp;
        private int __soluong;
        private double __giamgia;

        public ClassChiTietHDB()
        {
        }

        public ClassChiTietHDB(string mahdb, string masp, int soluong, double giamgia)
        {
            Mahdb = mahdb;
            Masp = masp;
            Soluong = soluong;
            Giamgia = giamgia;
        }

        public string Mahdb { get => __mahdb; set => __mahdb = value; }
        public string Masp { get => __masp; set => __masp = value; }
        public int Soluong { get => __soluong; set => __soluong = value; }
        public double Giamgia { get => __giamgia; set => __giamgia = value; }
    }
}
