﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QL_GiayDep
{
    public partial class frmBaoCao : Form
    {
        public frmBaoCao()
        {
            InitializeComponent();
        }

        private void btnThoat_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(comboBox1.SelectedIndex == 0)
            {
                frmBaoCaoHangTon f = new frmBaoCaoHangTon();
                f.MdiParent = this;
                f.Show();

            }
            if(comboBox1.SelectedIndex == 1)
            {
                frmBaoCaoHDNtheoQuy f = new frmBaoCaoHDNtheoQuy();
                f.MdiParent = this;
                f.Show();
            }
            if(comboBox1.SelectedIndex == 2)
            {
                frmBaoCaoHDBtheoNV f = new frmBaoCaoHDBtheoNV();
                f.MdiParent = this;
                f.Show();
            }
            if(comboBox1.SelectedIndex == 3)
            {
                frmBaoCaoTop3KHtheoQuy f = new frmBaoCaoTop3KHtheoQuy();
                f.MdiParent = this;
                f.Show();
            }
        }

        private void iconButton1_Click(object sender, EventArgs e)
        {
            if (comboBox1.SelectedIndex == 0)
            {
                frmBaoCaoHangTon f = new frmBaoCaoHangTon();
                f.MdiParent = this;
                f.Show();

            }
            if (comboBox1.SelectedIndex == 1)
            {
                frmBaoCaoHDNtheoQuy f = new frmBaoCaoHDNtheoQuy();
                f.MdiParent = this;
                f.Show();
            }
            if (comboBox1.SelectedIndex == 2)
            {
                frmBaoCaoHDBtheoNV f = new frmBaoCaoHDBtheoNV();
                f.MdiParent = this;
                f.Show();
            }
            if (comboBox1.SelectedIndex == 3)
            {
                frmBaoCaoTop3KHtheoQuy f = new frmBaoCaoTop3KHtheoQuy();
                f.MdiParent = this;
                f.Show();
            }
        }
    }
}
