﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QL_GiayDep
{
    class ClassNhanVien
    {
        private string __manv;
        private string __tennv;
        private string __gioitinh;
        private string __ngaysinh;
        private string __diachi;
        private string __macv;

        public ClassNhanVien()
        {
        }

        public ClassNhanVien(string manv, string tennv, string gioitinh, string ngaysinh, string diachi, string macv)
        {
            Manv = manv;
            Tennv = tennv;
            Gioitinh = gioitinh;
            Ngaysinh = ngaysinh;
            Diachi = diachi;
            Macv = macv;
        }

        public string Manv { get => __manv; set => __manv = value; }
        public string Tennv { get => __tennv; set => __tennv = value; }
        public string Gioitinh { get => __gioitinh; set => __gioitinh = value; }
        public string Ngaysinh { get => __ngaysinh; set => __ngaysinh = value; }
        public string Diachi { get => __diachi; set => __diachi = value; }
        public string Macv { get => __macv; set => __macv = value; }
    }
}
