﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data;

namespace QL_GiayDep
{
    public partial class frmNhanVien : Form
    {
        Modify modify;
        ClassNhanVien nhanVien;
        public frmNhanVien()
        {
            InitializeComponent();
        }

        private void frmNhanVien_Load(object sender, EventArgs e)
        {
            string query = "Select* from dbo.NhanVien";
            modify = new Modify();
            using (SqlConnection sqlConnection = Connection.GetSqlConnection())
            {
                sqlConnection.Open();
                SqlDataAdapter da = new SqlDataAdapter("Select* from dbo.CongViec", sqlConnection);//SQL là câu truy vấn bảng trong cơ sở dữ liệu, cn là connection đến cơ sở dữ liệu
                DataTable dt = new DataTable();
                da.Fill(dt);
                cbMaCV.DisplayMember = "MaCV";//Word là tên trường bạn muốn hiển thị trong combobox
                cbMaCV.ValueMember = "MaCV";
                cbMaCV.DataSource = dt;
                sqlConnection.Close();
            }
            try
            {
                dataGridView1.DataSource = modify.Table(query);
            }catch (Exception ex)
            {
                MessageBox.Show("Lỗi" + ex);
            }
            Clear_TextBoxes();
            Disable_Textboxes();
            Disable_2Buttons();
            Enable_3Buttons();

        }
        private void Disable_Textboxes()
        {
            cbMaCV.Enabled = false;
            cbMaCV.Text = "";
            txtMaNV.ReadOnly = true;
            txtTenNV.ReadOnly = true;
            txtGioiTinh.ReadOnly = true;
            dtpNgaySinh.Enabled = false;
            txtDiaChi.ReadOnly = true;
        }
        private void Enable_Textboxes()
        {
            cbMaCV.Enabled = true;
            txtMaNV.ReadOnly = false;
            txtTenNV.ReadOnly = false;
            txtGioiTinh.ReadOnly = false;
            dtpNgaySinh.Enabled = true;
            txtDiaChi.ReadOnly = false;
        }
        private void Enable_2Buttons()
        {
            btnHuy.Enabled = true;
            btnLuu.Enabled = true;
        }
        private void Disable_2Buttons()
        {
            btnHuy.Enabled = false;
            btnLuu.Enabled = false;
        }
        private void Enable_3Buttons()
        {
            btnThem.Enabled = true;
            btnSua.Enabled = true;
            btnXoa.Enabled = true;
        }
        private void Clear_TextBoxes()
        {
            txtMaNV.Text = "";
            txtTenNV.Text = "";
            cbMaCV.Text = "";
            txtGioiTinh.Text = "";
            txtDiaChi.Text = "";
            dtpNgaySinh.Value = DateTime.Now;
        }
        private bool CheckTextBoxes()
        {
            if (txtMaNV.Text.Trim() == "")//su dung trim khong lay khoang trang giua cac ki tu
            {
                MessageBox.Show("Vui lòng nhập mã nhân viên");
                return false;
            }
            if (txtTenNV.Text.Trim() == "")//su dung trim khong lay khoang trang giua cac ki tu
            {
                MessageBox.Show("Vui lòng nhập tên nhân viên");
                return false;
            }
            if (cbMaCV.Text.Trim() == "")//su dung trim khong lay khoang trang giua cac ki tu
            {
                MessageBox.Show("Vui long chon mã cong viec");
                return false;
            }
            if (txtGioiTinh.Text.Trim() == "")//su dung trim khong lay khoang trang giua cac ki tu
            {
                MessageBox.Show("Vui lòng nhập giới tính nhân viên");
                return false;
            }
            if (txtDiaChi.Text.Trim() == "")//su dung trim khong lay khoang trang giua cac ki tu
            {
                MessageBox.Show("Vui lòng nhập địa chỉ cua nhân viên");
                return false;
            }
            return true;
        }
        private void getValueTextBoxes()
        {

            string manv = this.txtMaNV.Text.Trim();
            string tennv = this.txtTenNV.Text.Trim();
            string gioitinh = this.txtGioiTinh.Text.Trim();
            string ngaysinh = dtpNgaySinh.Value.ToShortDateString();
            string diachi = this.txtDiaChi.Text.Trim();
            string macv = this.cbMaCV.Text.Trim();

            nhanVien = new ClassNhanVien(manv, tennv, gioitinh, ngaysinh, diachi, macv);
        }
        private string Query(string textBox)
        {
            if (textBox == "") return "null)";
            else return "'" + textBox + "')";
        }

        private void btnThoat_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            Enable_2Buttons();
            Enable_Textboxes();
            btnXoa.Enabled = false;
            btnSua.Enabled = false;
        }

        private void btnSua_Click(object sender, EventArgs e)
        {
            Enable_Textboxes();
            Enable_2Buttons();
            btnThem.Enabled = false;
            btnXoa.Enabled = false;
            txtMaNV.ReadOnly = true;
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            Disable_Textboxes();
            Enable_2Buttons();
            btnThem.Enabled = false;
            btnSua.Enabled = false;
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            if (CheckTextBoxes())
            {
                string query = "";
                if (!btnSua.Enabled && !btnXoa.Enabled)
                {
                    getValueTextBoxes();
                    query = "INSERT INTO dbo.NhanVien VALUES";
                    query += "('" +nhanVien.Manv + "',";
                    query += "'" + nhanVien.Tennv + "',";
                    query += "'" + nhanVien.Gioitinh + "',";
                    query += "'" + nhanVien.Ngaysinh + "',";
                    query += "'" + nhanVien.Diachi + "',";
                    query += Query(nhanVien.Macv);
                    try
                    {
                        if (MessageBox.Show("Bạn có muốn lưu không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
                        {
                            modify.setTable(query);
                            Clear_TextBoxes();
                            Enable_Textboxes();
                            MessageBox.Show("Thêm thành công");
                            frmNhanVien_Load(sender, e);
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("lỗi " + ex.Message);
                    }
                }
                if (!btnThem.Enabled && !btnXoa.Enabled)
                {
                    getValueTextBoxes();
                    query = "Update dbo.NhanVien SET ";
                    query += "Tennv = N'" + txtTenNV.Text + "',";
                    query += "Gioitinh = N'" + txtGioiTinh.Text + "',";
                    query += "NgaySinh = '" + dtpNgaySinh.Value.Date + "',";
                    query += "DiaChi = N'" + txtDiaChi.Text + "',";
                    query += "MaCV = '" + cbMaCV.Text + "'";
                    query += "Where MaNV = N'" + txtMaNV.Text + "'";
                    try
                    {
                        if (MessageBox.Show("Bạn có muốn lưu không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
                        {
                            modify.setTable(query);
                            Clear_TextBoxes();
                            Enable_Textboxes();
                            MessageBox.Show("Sửa thành công");
                            frmNhanVien_Load(sender, e);

                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("lỗi " + ex.Message);
                    }
                }
                if (!btnThem.Enabled && !btnSua.Enabled)
                {
                    getValueTextBoxes();
                    query = "Delete From dbo.NhanVien Where Manv =N'" + txtMaNV.Text + "'";
                    try
                    {
                        if (MessageBox.Show("Bạn có muốn lưu không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
                        {
                            modify.setTable(query);
                            MessageBox.Show("Xóa thành công");
                            frmNhanVien_Load(sender, e);
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("lỗi " + ex.Message);
                    }
                }

            }
        }

        private void btnHuy_Click(object sender, EventArgs e)
        {

            frmNhanVien_Load(sender, e);
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                int i = dataGridView1.CurrentRow.Index;
                txtMaNV.Text        = dataGridView1.Rows[i].Cells[0].Value.ToString();
                txtTenNV.Text       = dataGridView1.Rows[i].Cells[1].Value.ToString();
                txtGioiTinh.Text    = dataGridView1.Rows[i].Cells[2].Value.ToString();
                dtpNgaySinh.Text    = dataGridView1.Rows[i].Cells[3].Value.ToString();
                txtDiaChi.Text      = dataGridView1.Rows[i].Cells[4].Value.ToString();
                cbMaCV.Text         = dataGridView1.Rows[i].Cells[5].Value.ToString();

            }
            catch (Exception ex)
            {
                MessageBox.Show("lỗi " + ex.Message);
            }
        }

        private void btnLamMoi_Click(object sender, EventArgs e)
        {
            frmNhanVien_Load(sender, e);
        }
    }
}
