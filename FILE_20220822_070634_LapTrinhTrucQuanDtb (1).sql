﻿CREATE DATABASE C#_CuaHangGiayDep
USE C#_CuaHangGiayDep

--KHACHHANG--
CREATE TABLE dbo.Khachhang(
	MaKH		nvarchar(10) not null,	
	TenKH		nvarchar(40) null,
	DiaChi		nvarchar(50) null,
	SDT			nvarchar(20) null,
	constraint pk_kh primary key(MaKH)
)
CREATE TABLE dbo.Dangnhap(
	MaTK			nvarchar(10) not null,
	Taikhoan		nchar(10) not null,	
	Matkhau			nchar(10) not null,
	constraint pk_dn primary key(MaTK)
)

--NHANVIEN---
CREATE TABLE dbo.NhanVien(
	MaNV		nvarchar(10) not null,
	TenNV		nvarchar(40) not null,
	GioiTinh	nvarchar(10) null,
	NgaySinh	smalldatetime,
	DiaChi		nvarchar(50) null,
	MaCV		nvarchar(10) null,
	constraint pk_nv primary key(MaNV)
)
--THELOAI--
CREATE TABLE dbo.TheLoai(
	MaLoai		nvarchar(10) not null,
	TenLoai		nvarchar(40) not null,
	constraint pk_tl primary key(MaLoai)
)
--NHACUNGCAP-----
CREATE TABLE dbo.NhaCungCap(
	MaNCC			nvarchar(10) not null,
	TenNCC			nvarchar(40) not null,
	DiaChi			nvarchar(50) null,
	SDT				nvarchar(20) not null,
	constraint pk_ncc primary key(MaNCC)
)
--NUOC SAN XUAT--
CREATE TABLE dbo.NuocSanXuat(
	MaNSX			nvarchar(10) not null,
	TenNSX			nvarchar(40) not null
	constraint pk_nsx primary key(MaNSX)
)

--SANPHAM---
CREATE TABLE dbo.SanPham(
	MaSP			nvarchar(10) not null,
	TenSP			nvarchar(50) not null,
	MaLoai 			nvarchar(10) not null,
	MaSize 			nvarchar(10) not null,
	MaCL 			nvarchar(10) not null,
	MaMau 			nvarchar(10) not null,
	MaDT 			nvarchar(10) not null,
	MaMua 			nvarchar(10) not null,
	MaNSX 			nvarchar(10) not null,
	SoLuongNhap		int null,
	SoLuongBan		int null,
	SoLuongTon 		int null,
	HinhAnh 		image null,
	GiaNhap 		money not null,
	GiaBan 			money not null,

	constraint pk_sp primary key(MaSP)	
)
--CO--
CREATE TABLE dbo.Size(
	MaSize		nvarchar(10) not null,
	TenSize		nvarchar(40) not null,
	constraint pk_c primary key(MaSize)
)
--CHATLIEU--
CREATE TABLE dbo.ChatLieu(
	MaCL		nvarchar(10) not null,
	TenCL		nvarchar(40) not null,
	constraint pk_cl primary key(MaCL)
)
--DOITUONG--
CREATE TABLE dbo.DoiTuong(
	MaDT		nvarchar(10) not null,
	TenDT		nvarchar(40) not null,
	constraint pk_dt primary key(MaDT)
)

--MUA--
CREATE TABLE dbo.Mua(
	MaMua		nvarchar(10) not null,
	TenMua		nvarchar(40) not null,
	constraint pk_m primary key(MaMua)
)
--MAU--
CREATE TABLE dbo.Mau(
	MaMau		nvarchar(10) not null,
	TenMau		nvarchar(40) not null,
	constraint pk_mau primary key(MaMau)
)
--CONGVIEC--
CREATE TABLE dbo.CongViec(
	MaCV		nvarchar(10) not null,
	TenCV		nvarchar(40) not null,
	constraint pk_cv primary key(MaCV)
)
---HOADONNHAP---
CREATE TABLE dbo.HoaDonNhap(
	MaHDN			nvarchar(10) not null,
	MaNV			nvarchar(10) not null,
	NgayNhap 		smalldatetime	 not null, 
	MaNCC			nvarchar(10) not null,
	TongTien		money null,
	constraint pk_hdn primary key(MaHDN)
)
--HOADONBAN---
CREATE TABLE dbo.HoaDonBan(
	MaHDB			nvarchar(10) not null,
	MaNV			nvarchar(10) not null,
	NgayBan 		smalldatetime	 not null, 
	MaKH			nvarchar(10) not null,
	TongTien		money null,
	constraint pk_hdb primary key(MaHDB)
)
---CHITIETHOADONNHAP----
CREATE TABLE dbo.ChiTietHDN(
	MaHDN		nvarchar(10) not null,
	MaSP		nvarchar(10) not null,
	SLNhap		int not null,
	GiamGia	    float not null,
	ThanhTien	money null,
	constraint pk_cthdn primary key(MaHDN,MaSP)
)
--CHITIETHOADONBAN----
CREATE TABLE dbo.ChiTietHDB(
	MaHDB		nvarchar(10) not null,
	MaSP		nvarchar(10) not null,
	SLBan		int not null,
	GiamGia	    float not null,
	ThanhTien	money null,
	constraint pk_cthdb primary key(MaHDB,MaSP)
)
--KHOA NGOAI CHO BANG SanPham---
ALTER TABLE dbo.SanPham  WITH CHECK ADD  CONSTRAINT fk_SanPham_TheLoai FOREIGN KEY(MaLoai)
REFERENCES dbo.TheLoai (MaLoai)
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE dbo.SanPham  WITH CHECK ADD  CONSTRAINT fk_SanPham_Size FOREIGN KEY(MaSize)
REFERENCES dbo.Size (MaSize)
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE dbo.SanPham  WITH CHECK ADD  CONSTRAINT fk_SanPham_ChatLieu FOREIGN KEY(MaCL)
REFERENCES dbo.ChatLieu (MaCL)
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE dbo.SanPham  WITH CHECK ADD  CONSTRAINT fk_SanPham_Mau FOREIGN KEY(MaMau)
REFERENCES dbo.Mau (MaMau)
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE dbo.SanPham  WITH CHECK ADD  CONSTRAINT fk_SanPham_Mua FOREIGN KEY(MaMua)
REFERENCES dbo.Mua (MaMua)
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE dbo.SanPham  WITH CHECK ADD  CONSTRAINT fk_SanPham_DoiTuong FOREIGN KEY(MaDT)
REFERENCES dbo.DoiTuong (MaDT)
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE dbo.SanPham  WITH CHECK ADD  CONSTRAINT fk_SanPham_NuocSanXuat FOREIGN KEY(MaNSX)
REFERENCES dbo.NuocSanXuat (MaNSX)
ON UPDATE CASCADE
ON DELETE CASCADE
GO
--KHOA NGOAI CHO BANG HOA DON NHAP---
ALTER TABLE dbo.HoaDonNhap  WITH CHECK ADD  CONSTRAINT fk_HoaDonNhap_NhaCungCap FOREIGN KEY(MaNCC)
REFERENCES dbo.NhaCungCap (MaNCC)
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE dbo.HoaDonNhap  WITH CHECK ADD  CONSTRAINT fk_HoaDonNhap_NhanVien FOREIGN KEY(MaNV)
REFERENCES dbo.NhanVien (MaNV)
ON UPDATE CASCADE
ON DELETE CASCADE
GO
--KHOA NGOAI CHO BANG HOA DON BAN---
ALTER TABLE dbo.HoaDonBan WITH CHECK ADD  CONSTRAINT fk_HoaDonBan_Khachhang FOREIGN KEY(MaKH)
REFERENCES Khachhang (MaKH)
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE dbo.HoaDonBan  WITH CHECK ADD  CONSTRAINT fk_HoaDonBan_NhanVien FOREIGN KEY(MaNV)
REFERENCES dbo.NhanVien (MaNV)
ON UPDATE CASCADE
ON DELETE CASCADE
GO
--KHOA NGOAI CHO BANG ChiTietHDN
ALTER TABLE dbo.ChiTietHDN  WITH CHECK ADD  CONSTRAINT fk_ChiTietHDN_HoaDonNhap FOREIGN KEY(MaHDN)
REFERENCES HoaDonNhap (MaHDN)
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE dbo.ChiTietHDN  WITH CHECK ADD  CONSTRAINT fk_ChiTietHDN_SanPham FOREIGN KEY(MaSP)
REFERENCES dbo.SanPham (MaSP)
ON UPDATE CASCADE
ON DELETE CASCADE
GO
--KHOA NGOAI CHO BANG ChiTietHDB
ALTER TABLE dbo.ChiTietHDB  WITH CHECK ADD  CONSTRAINT fk_ChiTietHDB_HoaDonBan FOREIGN KEY(MaHDB)
REFERENCES HoaDonBan (MaHDB)
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE dbo.ChiTietHDB  WITH CHECK ADD  CONSTRAINT fk_ChiTietHDB_SanPham FOREIGN KEY(MaSP)
REFERENCES dbo.SanPham (MaSP)
ON UPDATE CASCADE
ON DELETE CASCADE
GO
--KHOA NGOAI CHO BANG NHANVIEN
ALTER TABLE dbo.NhanVien WITH CHECK ADD  CONSTRAINT fk_NhanVien_CongViec FOREIGN KEY(MaCV)
REFERENCES dbo.CongViec (MaCV)
ON UPDATE CASCADE
ON DELETE CASCADE
GO

--Công việc
INSERT [dbo].[CongViec] ([MaCV],[TenCV]) Values(N'CV01',N'Nhập hàng')
INSERT [dbo].[CongViec] ([MaCV],[TenCV]) Values(N'CV02',N'Bán hàng')
GO
--nhan vien
INSERT [dbo].[NhanVien] ([MaNV], [TenNV], [GioiTinh], [DiaChi],[NgaySinh],[MaCV] )
VALUES (N'MNV001', N'Hoàng Mạnh Tuân', N'Nam', N'Nam Định','1997-07-03 00:00:00',N'CV01')

INSERT [dbo].[NhanVien] ([MaNV], [TenNV], [GioiTinh], [DiaChi],[NgaySinh],[MaCV] )
VALUES (N'MNV002', N'Nguyễn Văn Ánh', N'Nam', N'Hà Nội','1995-07-13 00:00:00',N'CV01')

INSERT [dbo].[NhanVien] ([MaNV], [TenNV], [GioiTinh], [DiaChi],[NgaySinh],[MaCV] )
VALUES (N'MNV003', N'Nguyễn Thùy Linh', N'Nữ', N'Hà Nội','1998-11-03 00:00:00',N'CV02')

INSERT [dbo].[NhanVien] ([MaNV], [TenNV], [GioiTinh], [DiaChi],[NgaySinh],[MaCV] )
VALUES (N'MNV004', N'Hoàng Hà Vân', N'Nữ', N'Hà Nam','1997-06-22 00:00:00',N'CV02')

INSERT [dbo].[NhanVien] ([MaNV], [TenNV], [GioiTinh], [DiaChi],[NgaySinh],[MaCV] )
VALUES (N'MNV005', N'Hà Quang Mạnh', N'Nam', N'Bắc Ninh','1991-12-03 00:00:00',N'CV02')

INSERT [dbo].[NhanVien] ([MaNV], [TenNV], [GioiTinh], [DiaChi],[NgaySinh],[MaCV] )
VALUES (N'MNV006', N'Lê Thị Hòa', N'Nữ', N'Phú Thọ','1995-02-03 00:00:00',N'CV02')

INSERT [dbo].[NhanVien] ([MaNV], [TenNV], [GioiTinh], [DiaChi],[NgaySinh],[MaCV] )
VALUES (N'MNV007', N'Nguyễn Văn Trung', N'Nam', N'Lạng Sơn','1997-08-15 00:00:00',N'CV02')
GO
--Thay đổi để nhập sau--	
Go
INSERT [dbo].[TheLoai] ([MaLoai], [TenLoai]) Values(N'L01',N'Sandal cao gót')
INSERT [dbo].[TheLoai] ([MaLoai], [TenLoai]) Values(N'L02',N'Sneakers')
INSERT [dbo].[TheLoai] ([MaLoai], [TenLoai]) Values(N'L03',N'Giày lười')
INSERT [dbo].[TheLoai] ([MaLoai], [TenLoai]) Values(N'L04',N'Dép crocs')
INSERT [dbo].[TheLoai] ([MaLoai], [TenLoai]) Values(N'L05',N'Dép xỏ ngón')
INSERT [dbo].[TheLoai] ([MaLoai], [TenLoai]) Values(N'L06',N'Giày Derby')

GO
INSERT [dbo].[Size] ([MaSize], [TenSize]) Values(N'SIZE01',N'35')
INSERT [dbo].[Size] ([MaSize], [TenSize]) Values(N'SIZE02',N'36')
INSERT [dbo].[Size] ([MaSize], [TenSize]) Values(N'SIZE03',N'37')
INSERT [dbo].[Size] ([MaSize], [TenSize]) Values(N'SIZE04',N'38')
INSERT [dbo].[Size] ([MaSize], [TenSize]) Values(N'SIZE05',N'39')
INSERT [dbo].[Size] ([MaSize], [TenSize]) Values(N'SIZE06',N'40')
INSERT [dbo].[Size] ([MaSize], [TenSize]) Values(N'SIZE07',N'41')
INSERT [dbo].[Size] ([MaSize], [TenSize]) Values(N'SIZE08',N'42')
INSERT [dbo].[Size] ([MaSize], [TenSize]) Values(N'SIZE09',N'43')


GO
INSERT [dbo].[ChatLieu] ([MaCL], [TenCL]) Values(N'CL01',N'Da tổng hợp')
INSERT [dbo].[ChatLieu] ([MaCL], [TenCL]) Values(N'CL02',N'Da bò thật')
INSERT [dbo].[ChatLieu] ([MaCL], [TenCL]) Values(N'CL03',N'Nhựa')

Go
INSERT [dbo].[Mua] ([MaMua], [TenMua]) Values(N'MUA01',N'Xuân')
INSERT [dbo].[Mua] ([MaMua], [TenMua]) Values(N'MUA02',N'Hạ')
INSERT [dbo].[Mua] ([MaMua], [TenMua]) Values(N'MUA03',N'Thu')
INSERT [dbo].[Mua] ([MaMua], [TenMua]) Values(N'MUA04',N'Đông')
GO
INSERT [dbo].[NuocSanXuat] ([MaNSX], [TenNSX]) Values(N'NSX01',N'Việt Nam')
INSERT [dbo].[NuocSanXuat] ([MaNSX], [TenNSX]) Values(N'NSX02',N'Trung Quốc')

GO
INSERT [dbo].[Mau] ([MaMau], [TenMau]) Values(N'MM01',N'Đen')
INSERT [dbo].[Mau] ([MaMau], [TenMau]) Values(N'MM02',N'Xanh')
INSERT [dbo].[Mau] ([MaMau], [TenMau]) Values(N'MM03',N'Trắng')
INSERT [dbo].[Mau] ([MaMau], [TenMau]) Values(N'MM04',N'Nâu')
INSERT [dbo].[Mau] ([MaMau], [TenMau]) Values(N'MM05',N'Xám')
INSERT [dbo].[Mau] ([MaMau], [TenMau]) Values(N'MM06',N'Hồng')
GO
INSERT [dbo].[DoiTuong] ([MaDT], [TenDT]) Values(N'DT01',N'Nữ')
INSERT [dbo].[DoiTuong] ([MaDT], [TenDT]) Values(N'DT02',N'Nam')

GO
INSERT  [dbo].[SanPham]([MaSP], [TenSP],[MaLoai], [MaSize], [MaCL], [MaMau],[MaDT], [MaMua], [MaNSX],[HinhAnh],[GiaNhap],[GiaBan],[SoLuongNhap],[SoLuongBan],[SoLuongTon])
VALUES (N'SP001', N'GIÀY THỜI TRANG EG138',N'L01',N'SIZE01',N'CL01',N'MM01',N'DT01',N'MUA02',N'NSX01',NULL,800000, 880000,NULL,NULL,NULL)
GO
UPDATE SanPham SET [HinhAnh] = (SELECT MyImage.* from Openrowset(Bulk 'E:/Images/SP001.jpg', Single_Blob) MyImage)
 where MaSP= N'SP001'
Go

INSERT  [dbo].[SanPham]([MaSP], [TenSP],[MaLoai], [MaSize], [MaCL], [MaMau],[MaDT], [MaMua], [MaNSX],[HinhAnh],[GiaNhap],[GiaBan],[SoLuongNhap],[SoLuongBan],[SoLuongTon])
VALUES (N'SP002', N'GIÀY THỜI TRANG EG138',N'L01',N'SIZE01',N'CL01',N'MM02',N'DT01',N'MUA02',N'NSX01',NULL,8000000, 880000,NULL,NULL,NULL)
GO
UPDATE SanPham SET [HinhAnh] = (SELECT MyImage.* from Openrowset(Bulk 'E:/Images/SP002.jpg', Single_Blob) MyImage)
 where MaSP= N'SP002'
Go
INSERT  [dbo].[SanPham]([MaSP], [TenSP],[MaLoai], [MaSize], [MaCL], [MaMau],[MaDT], [MaMua], [MaNSX],[HinhAnh],[GiaNhap],[GiaBan],[SoLuongNhap],[SoLuongBan],[SoLuongTon])
VALUES (N'SP003', N'GIÀY THỜI TRANG EG135',N'L01',N'SIZE03',N'CL01',N'MM01',N'DT01',N'MUA03',N'NSX01',NULL,900000, 990000,NULL,NULL,NULL)
GO
UPDATE SanPham SET [HinhAnh] = (SELECT MyImage.* from Openrowset(Bulk 'E:/Images/SP003.jpg', Single_Blob) MyImage)
 where MaSP= N'SP003'
Go

INSERT  [dbo].[SanPham]([MaSP], [TenSP],[MaLoai], [MaSize], [MaCL], [MaMau],[MaDT], [MaMua], [MaNSX],[HinhAnh],[GiaNhap],[GiaBan],[SoLuongNhap],[SoLuongBan],[SoLuongTon])
VALUES (N'SP004', N'GIÀY SNEAKER HOMME – EGTM19',N'L02',N'SIZE04',N'CL02',N'MM01',N'DT02',N'MUA01',N'NSX02',NULL,1500000, 1650000,NULL,NULL,NULL)
GO
UPDATE SanPham SET [HinhAnh] = (SELECT MyImage.* from Openrowset(Bulk 'E:/Images/SP004.jpg', Single_Blob) MyImage)
 where MaSP= N'SP004'
Go

INSERT  [dbo].[SanPham]([MaSP], [TenSP],[MaLoai], [MaSize], [MaCL], [MaMau],[MaDT], [MaMua], [MaNSX],[HinhAnh],[GiaNhap],[GiaBan],[SoLuongNhap],[SoLuongBan],[SoLuongTon])
VALUES (N'SP005', N'GIÀY SNEAKER HOMME – EGTM19',N'L02',N'SIZE05',N'CL02',N'MM03',N'DT02',N'MUA01',N'NSX02',NULL,1500000, 1650000,NULL,NULL,NULL)
GO
UPDATE SanPham SET [HinhAnh] = (SELECT MyImage.* from Openrowset(Bulk 'E:/Images/SP005.jpg', Single_Blob) MyImage)
 where MaSP= N'SP005'
Go

INSERT  [dbo].[SanPham]([MaSP], [TenSP],[MaLoai], [MaSize], [MaCL], [MaMau],[MaDT], [MaMua], [MaNSX],[HinhAnh],[GiaNhap],[GiaBan],[SoLuongNhap],[SoLuongBan],[SoLuongTon])
VALUES (N'SP006', N'GIÀY HOMME – EGTM8',N'L06',N'SIZE05',N'CL02',N'MM04',N'DT02',N'MUA04',N'NSX02',NULL,1400000, 1540000,NULL,NULL,NULL)
GO
UPDATE SanPham SET [HinhAnh] = (SELECT MyImage.* from Openrowset(Bulk 'E:/Images/SP006.jpg', Single_Blob) MyImage)
 where MaSP= N'SP006'
Go
INSERT  [dbo].[SanPham]([MaSP], [TenSP],[MaLoai], [MaSize], [MaCL], [MaMau],[MaDT], [MaMua], [MaNSX],[HinhAnh],[GiaNhap],[GiaBan],[SoLuongNhap],[SoLuongBan],[SoLuongTon])
VALUES (N'SP007', N'GIÀY HOMME – EGTM8',N'L06',N'SIZE05',N'CL02',N'MM01',N'DT02',N'MUA04',N'NSX02',NULL,2100000, 2310000,NULL,NULL,NULL)
GO
UPDATE SanPham SET [HinhAnh] = (SELECT MyImage.* from Openrowset(Bulk 'E:/Images/SP007.jpg', Single_Blob) MyImage)
 where MaSP= N'SP007'
Go
INSERT  [dbo].[SanPham]([MaSP], [TenSP],[MaLoai], [MaSize], [MaCL], [MaMau],[MaDT], [MaMua], [MaNSX],[HinhAnh],[GiaNhap],[GiaBan],[SoLuongNhap],[SoLuongBan],[SoLuongTon])
VALUES (N'SP008', N'Dép ELLY HOMME – EGTM16',N'L05',N'SIZE05',N'CL02',N'MM04',N'DT02',N'MUA02',N'NSX01',NULL,990000, 1089000,NULL,NULL,NULL)
GO
UPDATE SanPham SET [HinhAnh] = (SELECT MyImage.* from Openrowset(Bulk 'E:/Images/SP008.jpg', Single_Blob) MyImage)
 where MaSP= N'SP008'
Go

INSERT  [dbo].[SanPham]([MaSP], [TenSP],[MaLoai], [MaSize], [MaCL], [MaMau],[MaDT], [MaMua], [MaNSX],[HinhAnh],[GiaNhap],[GiaBan],[SoLuongNhap],[SoLuongBan],[SoLuongTon])
VALUES (N'SP009', N'Dép ELLY HOMME – EGTM17',N'L05',N'SIZE05',N'CL02',N'MM04',N'DT02',N'MUA03',N'NSX01',NULL,960000, 1092000,NULL,NULL,NULL)
GO
UPDATE SanPham SET [HinhAnh] = (SELECT MyImage.* from Openrowset(Bulk 'E:/Images/SP009.jpg', Single_Blob) MyImage)
 where MaSP= N'SP009'
Go
INSERT  [dbo].[SanPham]([MaSP], [TenSP],[MaLoai], [MaSize], [MaCL], [MaMau],[MaDT], [MaMua], [MaNSX],[HinhAnh],[GiaNhap],[GiaBan],[SoLuongNhap],[SoLuongBan],[SoLuongTon])
VALUES (N'SP010', N'Dép ELLY HOMME – EGTM17',N'L05',N'SIZE07',N'CL02',N'MM04',N'DT02',N'MUA03',N'NSX01',NULL,960000,1092000,NULL,NULL,NULL)
GO
UPDATE SanPham SET [HinhAnh] = (SELECT MyImage.* from Openrowset(Bulk 'E:/Images/SP009.jpg', Single_Blob) MyImage)
 where MaSP= N'SP010'
Go
INSERT  [dbo].[SanPham]([MaSP], [TenSP],[MaLoai], [MaSize], [MaCL], [MaMau],[MaDT], [MaMua], [MaNSX],[HinhAnh],[GiaNhap],[GiaBan],[SoLuongNhap],[SoLuongBan],[SoLuongTon])
VALUES (N'SP011', N'Dép ELLY HOMME – EGTM18',N'L05',N'SIZE08',N'CL02',N'MM01',N'DT01',N'MUA02',N'NSX01',NULL,760000, 836000,NULL,NULL,NULL)
GO
UPDATE SanPham SET [HinhAnh] = (SELECT MyImage.* from Openrowset(Bulk 'E:/Images/SP011.jpg', Single_Blob) MyImage)
 where MaSP= N'SP011'
Go
INSERT  [dbo].[SanPham]([MaSP], [TenSP],[MaLoai], [MaSize], [MaCL], [MaMau],[MaDT], [MaMua], [MaNSX],[HinhAnh],[GiaNhap],[GiaBan],[SoLuongNhap],[SoLuongBan],[SoLuongTon])
VALUES (N'SP012', N'Dép Crocs Classic All Terrain',N'L04',N'SIZE03',N'CL03',N'MM05',N'DT01',N'MUA02',N'NSX02',NULL,3600000, 396000)
GO
UPDATE SanPham SET [HinhAnh] = (SELECT MyImage.* from Openrowset(Bulk 'E:/Images/SP012.jpg', Single_Blob) MyImage)
 where MaSP= N'SP012'
Go

INSERT  [dbo].[SanPham]([MaSP], [TenSP],[MaLoai], [MaSize], [MaCL], [MaMau],[MaDT], [MaMua], [MaNSX],[HinhAnh],[GiaNhap],[GiaBan],[SoLuongNhap],[SoLuongBan],[SoLuongTon])
VALUES (N'SP013', N'Giày thời trang ELLY – EG115',N'L01',N'SIZE06',N'CL01',N'MM06',N'DT01',N'MUA01',N'NSX02',NULL,860000,946000,NULL,NULL,NULL)
GO
UPDATE SanPham SET [HinhAnh] = (SELECT MyImage.* from Openrowset(Bulk 'E:/Images/SP013.jpg', Single_Blob) MyImage)
 where MaSP= N'SP013'
Go
INSERT  [dbo].[SanPham]([MaSP], [TenSP],[MaLoai], [MaSize], [MaCL], [MaMau],[MaDT], [MaMua], [MaNSX],[HinhAnh],[GiaNhap],[GiaBan],[SoLuongNhap],[SoLuongBan],[SoLuongTon])
VALUES (N'SP014', N'Giày thời trang ELLY – EG115',N'L01',N'SIZE04',N'CL01',N'MM01',N'DT01',N'MUA01',N'NSX02',NULL,860000, 946000,NULL,NULL,NULL)
GO
UPDATE SanPham SET [HinhAnh] = (SELECT MyImage.* from Openrowset(Bulk 'E:/Images/SP014.jpg', Single_Blob) MyImage)
 where MaSP= N'SP014'
Go
INSERT  [dbo].[SanPham]([MaSP], [TenSP],[MaLoai], [MaSize], [MaCL], [MaMau],[MaDT], [MaMua], [MaNSX],[HinhAnh],[GiaNhap],[GiaBan],[SoLuongNhap],[SoLuongBan],[SoLuongTon])
VALUES (N'SP015', N'Giày ELLY – EG142',N'L01',N'SIZE02',N'CL01',N'MM06',N'DT01',N'MUA04',N'NSX01',NULL,760000, 836000,NULL,NULL,NULL)
GO
UPDATE SanPham SET [HinhAnh] = (SELECT MyImage.* from Openrowset(Bulk 'E:/Images/SP015.jpg', Single_Blob) MyImage)
 where MaSP= N'SP015'
Go

INSERT  [dbo].[SanPham]([MaSP], [TenSP],[MaLoai], [MaSize], [MaCL], [MaMau],[MaDT], [MaMua], [MaNSX],[HinhAnh],[GiaNhap],[GiaBan],[SoLuongNhap],[SoLuongBan],[SoLuongTon])
VALUES (N'SP016', N'Giày ELLY HOMME – EGTM13',N'L06',N'SIZE09',N'CL02',N'MM04',N'DT02',N'MUA04',N'NSX01',NULL,1760000,1936000 ,NULL,NULL,NULL)
GO
UPDATE SanPham SET [HinhAnh] = (SELECT MyImage.* from Openrowset(Bulk 'E:/Images/SP016.jpg', Single_Blob) MyImage)
 where MaSP= N'SP016'
Go

INSERT  [dbo].[SanPham]([MaSP], [TenSP],[MaLoai], [MaSize], [MaCL], [MaMau],[MaDT], [MaMua], [MaNSX],[HinhAnh],[GiaNhap],[GiaBan],[SoLuongNhap],[SoLuongBan],[SoLuongTon])
VALUES (N'SP017', N'Giày Penny Loafer GNLAMJ238-F1-D',N'L03',N'SIZE09',N'CL02',N'MM01',N'DT02',N'MUA04',N'NSX02',NULL,1900000, 2090000,NULL,NULL,NULL)
GO
UPDATE SanPham SET [HinhAnh] = (SELECT MyImage.* from Openrowset(Bulk 'E:/Images/SP017.jpg', Single_Blob) MyImage)
 where MaSP= N'SP017'
Go
INSERT  [dbo].[SanPham]([MaSP], [TenSP],[MaLoai], [MaSize], [MaCL], [MaMau],[MaDT], [MaMua], [MaNSX],[HinhAnh],[GiaNhap],[GiaBan],[SoLuongNhap],[SoLuongBan],[SoLuongTon])
VALUES (N'SP018', N'Giày họa tiết kẻ ca rô GNLA12996-D',N'L03',N'SIZE07',N'CL02',N'MM01',N'DT02',N'MUA01',N'NSX02',NULL,1560000,1716000,NULL,NULL,NULL )
GO
UPDATE SanPham SET [HinhAnh] = (SELECT MyImage.* from Openrowset(Bulk 'E:/Images/SP018.jpg', Single_Blob) MyImage)
 where MaSP= N'SP018'
Go
INSERT  [dbo].[SanPham]([MaSP], [TenSP],[MaLoai], [MaSize], [MaCL], [MaMau],[MaDT], [MaMua], [MaNSX],[HinhAnh],[GiaNhap],[GiaBan],[SoLuongNhap],[SoLuongBan],[SoLuongTon])
VALUES (N'SP019', N'Giày họa tiết kẻ ca rô GNLA12996-D',N'L03',N'SIZE08',N'CL02',N'MM01',N'DT02',N'MUA01',N'NSX02',NULL,1560000, 1716000,NULL,NULL,NULL)
GO
UPDATE SanPham SET [HinhAnh] = (SELECT MyImage.* from Openrowset(Bulk 'E:/Images/SP018.jpg', Single_Blob) MyImage)
 where MaSP= N'SP019'
Go
INSERT  [dbo].[SanPham]([MaSP], [TenSP],[MaLoai], [MaSize], [MaCL], [MaMau],[MaDT], [MaMua], [MaNSX],[HinhAnh],[GiaNhap],[GiaBan],[SoLuongNhap],[SoLuongBan],[SoLuongTon])
VALUES (N'SP020', N'Giày họa tiết kẻ ca rô GNLA12996-D',N'L03',N'SIZE09',N'CL02',N'MM01',N'DT02',N'MUA01',N'NSX02',NULL,1560000, 1716000,NULL,NULL,NULL)
GO
UPDATE SanPham SET [HinhAnh] = (SELECT MyImage.* from Openrowset(Bulk 'E:/Images/SP018.jpg', Single_Blob) MyImage)
 where MaSP= N'SP020'
Go
INSERT  [dbo].[SanPham]([MaSP], [TenSP],[MaLoai], [MaSize], [MaCL], [MaMau],[MaDT], [MaMua], [MaNSX],[HinhAnh],[GiaNhap],[GiaBan],[SoLuongNhap],[SoLuongBan],[SoLuongTon])
VALUES (N'SP021', N'Giày Bit Loafer họa tiết đan chéo GNLA2235-D',N'L03',N'SIZE08',N'CL02',N'MM01',N'DT02',N'MUA03',N'NSX02',NULL,1360000, 1496000,NULL,NULL,NULL)
GO
UPDATE SanPham SET [HinhAnh] = (SELECT MyImage.* from Openrowset(Bulk 'E:/Images/SP021.jpg', Single_Blob) MyImage)
 where MaSP= N'SP021'
Go
INSERT  [dbo].[SanPham]([MaSP], [TenSP],[MaLoai], [MaSize], [MaCL], [MaMau],[MaDT], [MaMua], [MaNSX],[HinhAnh],[GiaNhap],[GiaBan],[SoLuongNhap],[SoLuongBan],[SoLuongTon])
VALUES (N'SP022', N'Dép Sabo Nam Literide Mesh Mule',N'L04',N'SIZE08',N'CL03',N'MM01',N'DT02',N'MUA03',N'NSX02',NULL,3650000, 4015000,NULL,NULL,NULL)
GO
UPDATE SanPham SET [HinhAnh] = (SELECT MyImage.* from Openrowset(Bulk 'E:/Images/SP022.png', Single_Blob) MyImage)
 where MaSP= N'SP022'
Go

INSERT  [dbo].[SanPham]([MaSP], [TenSP],[MaLoai], [MaSize], [MaCL], [MaMau],[MaDT], [MaMua], [MaNSX],[HinhAnh],[GiaNhap],[GiaBan],[SoLuongNhap],[SoLuongBan],[SoLuongTon])
VALUES (N'SP023', N'NIKE AIRMAX 97 SE CORK',N'L02',N'SIZE07',N'CL01',N'MM02',N'DT02',N'MUA03',N'NSX02',NULL,23650000, 2601500,NULL,NULL,NULL)
GO
UPDATE SanPham SET [HinhAnh] = (SELECT MyImage.* from Openrowset(Bulk 'E:/Images/SP023.jpg', Single_Blob) MyImage)
 where MaSP= N'SP023'
Go

INSERT  [dbo].[SanPham]([MaSP], [TenSP],[MaLoai], [MaSize], [MaCL], [MaMau],[MaDT], [MaMua], [MaNSX],[HinhAnh],[GiaNhap],[GiaBan],[SoLuongNhap],[SoLuongBan],[SoLuongTon])
VALUES (N'SP024', N'NIKE AIRMAX AXIS TRIPLEWHITE',N'L02',N'SIZE08',N'CL01',N'MM03',N'DT02',N'MUA03',N'NSX02',NULL,1365000, 1601500,NULL,NULL,NULL)
GO
UPDATE SanPham SET [HinhAnh] = (SELECT MyImage.* from Openrowset(Bulk 'E:/Images/SP024.jpg', Single_Blob) MyImage)
 where MaSP= N'SP024'
Go
INSERT  [dbo].[SanPham]([MaSP], [TenSP],[MaLoai], [MaSize], [MaCL], [MaMau],[MaDT], [MaMua], [MaNSX],[HinhAnh],[GiaNhap],[GiaBan],[SoLuongNhap],[SoLuongBan],[SoLuongTon])
VALUES (N'SP025', N'NIKE AIRMAX AXIS TRIPLEWHITE',N'L02',N'SIZE09',N'CL01',N'MM03',N'DT02',N'MUA03',N'NSX02',NULL,1365000, 1601500,NULL,NULL,NULL)
GO
UPDATE SanPham SET [HinhAnh] = (SELECT MyImage.* from Openrowset(Bulk 'E:/Images/SP024.jpg', Single_Blob) MyImage)
 where MaSP= N'SP025'
Go
--Nha cung cap--
INSERT [dbo].[NhaCungCap] ([MaNCC], [TenNCC], [DiaChi], [SDT]) VALUES (N'NCC01', N'CÔNG TY CỔ PHẦN THỜI TRANG GLAMOR',N'Cầu Giấy, TP.Hà Nội', '0243877777')
INSERT [dbo].[NhaCungCap] ([MaNCC], [TenNCC], [DiaChi], [SDT]) VALUES (N'NCC02', N'CÔNG TY CỔ PHẦN BOSSGIAY',N'Thủ Đức, TP.HCM', '0983728392')
INSERT [dbo].[NhaCungCap] ([MaNCC], [TenNCC], [DiaChi], [SDT]) VALUES (N'NCC03', N'CÔNG TY CỔ PHẦN GIÀY DÉP THÁI MINH',N'Hoàng Mai, TP.Hà Nội', '02438483920')
go
--TRIGGER ràng buộc số lượng bán và nhập và tính hàng tôn
CREATE TRIGGER RangBuocSLBanvaSLNhaptinhHangTon
on dbo.SanPham FOR INSERT,UPDATE
AS
BEGIN
	DECLARE @SLNHAP INT
	DECLARE @SLBAN INT
	SELECT @SLBAN = inserted.SoLuongBan FROM inserted
	SELECT @SLNHAP = inserted.SoLuongNhap FROM inserted
	IF(@SLBAN >= @SLNHAP)
		BEGIN
			PRINT('CAN THEM PHAN TU SAO CHO SO LUONG BAN RA LON HON SO LUONG NHAP')
			ROLLBACK TRAN;
		END
	ELSE
		BEGIN
			UPDATE dbo.SanPham SET SoLuongTon = @SLNHAP - @SLBAN 
			FROM dbo.SanPham,inserted 
			WHERE inserted.MaSP = dbo.SanPham.MaSP
		END
END


--Hoa đơn nhập
create trigger TG_HoaDonNhap_TongTien_Cus on dbo.ChiTietHDN
for insert,update,delete
as
begin
declare @gianhap money,@mahdn nvarchar(10), @masp nvarchar(10),@slnhap1 int,@slnhap2 int
select @mahdn=MaHDN, @masp=MaSP, @slnhap1=SLNhap from inserted
select @mahdn=MaHDN, @masp=MaSP, @slnhap2=SLNhap from deleted
select @gianhap=GiaNhap from DBO.SanPham where MaSP=@masp
update dbo.HoaDonNhap set TongTien = isnull(TongTien, 0) + isnull(@gianhap*@slnhap1,0) - isnull(@gianhap*@slnhap2,0) where MaHDN=@mahdn
end

INSERT [dbo].[HoaDonNhap]([MaHDN], [NgayNhap], [MaNCC], [MaNV],[TongTien]) 
VALUES (N'HDN001', N'2021-08-17 16:20:00', N'NCC01',N'MNV002',NULL)
INSERT [dbo].[HoaDonNhap]([MaHDN], [NgayNhap], [MaNCC], [MaNV],[TongTien]) 
VALUES (N'HDN002',N'2021-08-23 18:30:00', N'NCC03',N'MNV001',NULL)
INSERT [dbo].[HoaDonNhap]([MaHDN], [NgayNhap], [MaNCC], [MaNV],[TongTien]) 
VALUES (N'HDN003',N'2021-08-25 09:00:00', N'NCC01',N'MNV002',NULL)
INSERT [dbo].[HoaDonNhap]([MaHDN], [NgayNhap], [MaNCC], [MaNV],[TongTien]) 
VALUES (N'HDN004', N'2021-09-17 06:00:00', N'NCC01',N'MNV001',NULL)
INSERT [dbo].[HoaDonNhap]([MaHDN], [NgayNhap], [MaNCC], [MaNV],[TongTien]) 
VALUES (N'HDN005',N'2021-09-26 09:00:00', N'NCC02',N'MNV002',NULL)
 go
 --Chi tiết HDN-- 5 nghĩa là 5%
create trigger TG_HoaDonNhap_TongSLNhap on dbo.ChiTietHDN
for DELETE,INSERT,UPDATE
as
begin
declare @masp nvarchar(10),@slnhap1 INT,@slnhap2 INT
IF EXISTS(SELECT * FROM Inserted)
BEGIN
select @masp=MaSP, @slnhap1= SLNhap from inserted
END
IF EXISTS(SELECT * FROM Deleted)
BEGIN
select @masp=MaSP, @slnhap2=SLnhap from deleted
END
update dbo.SanPham set SoLuongNhap = isnull(SoLuongNhap, 0)+ ISNULL(@slnhap1,0)- ISNULL(@slnhap2,0) WHERE dbo.SanPham.MaSP=@masp
end
GO

CREATE TRIGGER TrgUpdateThanhTienNhap ON dbo.ChiTietHDN
FOR INSERT, UPDATE
AS
BEGIN
	declare @mahdn char(10), @masp char(10), @gianhap money, @giamgia float
	select @mahdn=MaHDN, @masp=MaSP, @giamgia = GiamGia from inserted
	select @gianhap=GiaNhap from dbo.SanPham where MaSP=@masp
	update dbo.ChiTietHDN set ThanhTien=@gianhap*SLNhap - (@gianhap*SLNhap*(GiamGia/100)) where MaHDN=@mahdn and MaSP=@masp
END

 INSERT [dbo].[ChiTietHDN]([MaHDN], [MaSP], [SLNhap],[GiamGia],[ThanhTien]) VALUES (N'HDN001', N'SP001',10,5,NULL)
 INSERT [dbo].[ChiTietHDN]([MaHDN], [MaSP], [SLNhap],[GiamGia],[ThanhTien]) VALUES (N'HDN001', N'SP002',10,7.5,NULL)
INSERT [dbo].[ChiTietHDN]([MaHDN], [MaSP], [SLNhap],[GiamGia],[ThanhTien]) VALUES (N'HDN001', N'SP003',10,6.5,NULL)
INSERT [dbo].[ChiTietHDN]([MaHDN], [MaSP], [SLNhap],[GiamGia],[ThanhTien]) VALUES (N'HDN001', N'SP004',10,5,NULL)
INSERT [dbo].[ChiTietHDN]([MaHDN], [MaSP], [SLNhap],[GiamGia],[ThanhTien]) VALUES (N'HDN001', N'SP005',10,5,NULL)
INSERT [dbo].[ChiTietHDN]([MaHDN], [MaSP], [SLNhap],[GiamGia],[ThanhTien]) VALUES (N'HDN001', N'SP006',10,5,NULL)
INSERT [dbo].[ChiTietHDN]([MaHDN], [MaSP], [SLNhap],[GiamGia],[ThanhTien]) VALUES (N'HDN002', N'SP007',10,7.5,NULL)
INSERT [dbo].[ChiTietHDN]([MaHDN], [MaSP], [SLNhap],[GiamGia],[ThanhTien]) VALUES (N'HDN002', N'SP008',10,7.5,NULL)
INSERT [dbo].[ChiTietHDN]([MaHDN], [MaSP], [SLNhap],[GiamGia],[ThanhTien]) VALUES (N'HDN002', N'SP009',10,7.5,NULL)
INSERT [dbo].[ChiTietHDN]([MaHDN], [MaSP], [SLNhap],[GiamGia],[ThanhTien]) VALUES (N'HDN002', N'SP010',10,7.5,NULL)
INSERT [dbo].[ChiTietHDN]([MaHDN], [MaSP], [SLNhap],[GiamGia],[ThanhTien]) VALUES (N'HDN003', N'SP011',14,5,NULL)
INSERT [dbo].[ChiTietHDN]([MaHDN], [MaSP], [SLNhap],[GiamGia],[ThanhTien]) VALUES (N'HDN003', N'SP012',14,5,NULL)
INSERT [dbo].[ChiTietHDN]([MaHDN], [MaSP], [SLNhap],[GiamGia],[ThanhTien]) VALUES (N'HDN003', N'SP013',14,5,NULL)
INSERT [dbo].[ChiTietHDN]([MaHDN], [MaSP], [SLNhap],[GiamGia],[ThanhTien]) VALUES (N'HDN003', N'SP014',14,10.5,NULL)
INSERT [dbo].[ChiTietHDN]([MaHDN], [MaSP], [SLNhap],[GiamGia],[ThanhTien]) VALUES (N'HDN003', N'SP015',14,5,NULL)
INSERT [dbo].[ChiTietHDN]([MaHDN], [MaSP], [SLNhap],[GiamGia],[ThanhTien]) VALUES (N'HDN003', N'SP016',14,5,NULL)
INSERT [dbo].[ChiTietHDN]([MaHDN], [MaSP], [SLNhap],[GiamGia],[ThanhTien]) VALUES (N'HDN003', N'SP017',14,8,NULL)
INSERT [dbo].[ChiTietHDN]([MaHDN], [MaSP], [SLNhap],[GiamGia],[ThanhTien]) VALUES (N'HDN003', N'SP018',14,5,NULL)
INSERT [dbo].[ChiTietHDN]([MaHDN], [MaSP], [SLNhap],[GiamGia],[ThanhTien]) VALUES (N'HDN003', N'SP019',14,5,NULL)
INSERT [dbo].[ChiTietHDN]([MaHDN], [MaSP], [SLNhap],[GiamGia],[ThanhTien]) VALUES (N'HDN003', N'SP020',14,6,NULL)
INSERT [dbo].[ChiTietHDN]([MaHDN], [MaSP], [SLNhap],[GiamGia],[ThanhTien]) VALUES (N'HDN004', N'SP021',12,7,NULL)
INSERT [dbo].[ChiTietHDN]([MaHDN], [MaSP], [SLNhap],[GiamGia],[ThanhTien]) VALUES (N'HDN004', N'SP022',12,7,NULL)
INSERT [dbo].[ChiTietHDN]([MaHDN], [MaSP], [SLNhap],[GiamGia],[ThanhTien]) VALUES (N'HDN004', N'SP023',12,7,NULL)
INSERT [dbo].[ChiTietHDN]([MaHDN], [MaSP], [SLNhap],[GiamGia],[ThanhTien]) VALUES (N'HDN004', N'SP024',12,6.7,NULL)
INSERT [dbo].[ChiTietHDN]([MaHDN], [MaSP], [SLNhap],[GiamGia],[ThanhTien]) VALUES (N'HDN004', N'SP025',12,7,NULL)
INSERT [dbo].[ChiTietHDN]([MaHDN], [MaSP], [SLNhap],[GiamGia],[ThanhTien]) VALUES (N'HDN004', N'SP003',12,7,NULL)
INSERT [dbo].[ChiTietHDN]([MaHDN], [MaSP], [SLNhap],[GiamGia],[ThanhTien]) VALUES (N'HDN004', N'SP004',12,7,NULL)
INSERT [dbo].[ChiTietHDN]([MaHDN], [MaSP], [SLNhap],[GiamGia],[ThanhTien]) VALUES (N'HDN005', N'SP013',5,7,NULL)
INSERT [dbo].[ChiTietHDN]([MaHDN], [MaSP], [SLNhap],[GiamGia],[ThanhTien]) VALUES (N'HDN005', N'SP024',2,7,NULL)
INSERT [dbo].[ChiTietHDN]([MaHDN], [MaSP], [SLNhap],[GiamGia],[ThanhTien]) VALUES (N'HDN005', N'SP017',7,7,NULL)
INSERT [dbo].[ChiTietHDN]([MaHDN], [MaSP], [SLNhap],[GiamGia],[ThanhTien]) VALUES (N'HDN005', N'SP005',10,10.6,NULL)
Go
--Khsch hàng
INSERT [dbo].[KhachHang] ([MaKH], [TenKH], [DiaChi], [SDT]) VALUES (N'MKH001',N'Tran Phương Anh',N'Nam Từ Liêm - Hà Nội',N'0912568765')
INSERT [dbo].[KhachHang] ([MaKH], [TenKH], [DiaChi], [SDT]) values(N'MKH021',N'Trần Tuấn Anh',N'59 Đường Mễ Trì-HN',N'0912568765')
INSERT [dbo].[KhachHang] ([MaKH], [TenKH], [DiaChi], [SDT]) values(N'MKH002',N'Alvaro Silva',N'1 Phố Lưu Hữu Phước-HN',N'0856345742')
INSERT [dbo].[KhachHang] ([MaKH], [TenKH], [DiaChi], [SDT]) values(N'MKH003',N'Nguyễn Duy Anh',N'175 Phố Tây Sơn-HN',N'0336627415')
INSERT [dbo].[KhachHang] ([MaKH], [TenKH], [DiaChi], [SDT]) values(N'MKH004',N'Bùi Đức Đông',N'Hà Đông-HN',N'0845323477')
INSERT [dbo].[KhachHang] ([MaKH], [TenKH], [DiaChi], [SDT]) values(N'MKH005',N'Phan Thế Anh',N'180 Đội Cấn-HN',N'0387874862')
INSERT [dbo].[KhachHang] ([MaKH], [TenKH], [DiaChi], [SDT]) values(N'MKH006',N'Bùi Vân Anh',N'Thanh Oai-HN',N'0967823355')
INSERT [dbo].[KhachHang] ([MaKH], [TenKH], [DiaChi], [SDT]) values(N'MKH007',N'Mai Đình Chinh',N'Hải Hậu-Nam Định',N'0346756564')
INSERT [dbo].[KhachHang] ([MaKH], [TenKH], [DiaChi], [SDT]) values(N'MKH008',N'Phạm Thu Uyên',N'Duy Tiên-Hà Nam',N'0934566575')
INSERT [dbo].[KhachHang] ([MaKH], [TenKH], [DiaChi], [SDT]) values(N'MKH009',N'Phan Khánh Duy',N' Đường An Dương Vương-HN', N'0926315673')
INSERT [dbo].[KhachHang] ([MaKH], [TenKH], [DiaChi], [SDT]) values(N'MKH010',N'Nguyễn Thị Hương',N'60 Phố Ngọc Hà-HN',N'0962632543')
INSERT [dbo].[KhachHang] ([MaKH], [TenKH], [DiaChi], [SDT]) values(N'MKH011',N'Nguyễn Thị Hà',N'3 Ngõ Yên Thế-HN',N'0876688974')
INSERT [dbo].[KhachHang] ([MaKH], [TenKH], [DiaChi], [SDT]) values(N'MKH012',N'Nguyễn Trung Hiếu',N'160 Phố Nguyễn Sơn-HN',N'077635245')
INSERT [dbo].[KhachHang] ([MaKH], [TenKH], [DiaChi], [SDT]) values(N'MKH013',N'Bùi Văn Dũng',N'Khu đô thị Việt Hưng-HN',N'0999467723')
INSERT [dbo].[KhachHang] ([MaKH], [TenKH], [DiaChi], [SDT]) values(N'MKH014',N'Nguyễn Quang Tùng',N'Chúc Sơn-Chương Mỹ-HN',N'0398201409')
INSERT [dbo].[KhachHang] ([MaKH], [TenKH], [DiaChi], [SDT]) values(N'MKH015',N'Nguyễn Minh Hà',N'30 Phố Lý Tự Trọng-HN',N'0857351234')
INSERT [dbo].[KhachHang] ([MaKH], [TenKH], [DiaChi], [SDT]) values(N'MKH016',N'Nguyễn Thị Thu Quỳnh',N'19 Đường Văn Tiến Dũng-HN',N'0851347881')
INSERT [dbo].[KhachHang] ([MaKH], [TenKH], [DiaChi], [SDT]) values(N'MKH017',N'Tạ Quang Tuấn',N'559 Phố Kim Ngưu-HN',N'078383367')
INSERT [dbo].[KhachHang] ([MaKH], [TenKH], [DiaChi], [SDT]) values(N'MKH018',N'Lê Nhật Lệ',N'443 Đường Phạm Văn Đồng-HN',N'0954267655')
INSERT [dbo].[KhachHang] ([MaKH], [TenKH], [DiaChi], [SDT]) values(N'MKH019',N'',N'916 Đường Bạch Đằng-HN',N'0843256640')
INSERT [dbo].[KhachHang] ([MaKH], [TenKH], [DiaChi], [SDT]) values(N'MKH020',N'Vũ Thị Huệ',N'17 Phố Huế-HN',N'0936378219')
INSERT [dbo].[KhachHang] ([MaKH], [TenKH], [DiaChi], [SDT]) values(N'MKH021',N'Vũ Thị Thanh',N'TP.Hải Dương',N'0983625268')
INSERT [dbo].[KhachHang] ([MaKH], [TenKH], [DiaChi], [SDT]) values(N'MKH022',N'Nguyễn Trọng Hoàng',N'TP.Vinh-NA',N'0927482921')
INSERT [dbo].[KhachHang] ([MaKH], [TenKH], [DiaChi], [SDT]) values(N'MKH023',N'Nguyễn Lan Anh',N'401 Cổ Nhuế',N'0936271829')
INSERT [dbo].[KhachHang] ([MaKH], [TenKH], [DiaChi], [SDT]) values(N'MKH024',N'Phạm Thu Hà',N'Phủ Lý-Hà Nam',N'0910284627')
INSERT [dbo].[KhachHang] ([MaKH], [TenKH], [DiaChi], [SDT]) values(N'MKH025',N'Phạm Đức Thắng',N'Hải Hậu-Nam Định',N'0945279193')
GO
--Hoa don ban
create trigger TG_HoaDonBan_TongTien_Cus on dbo.ChiTietHDB
for insert,update,delete
as
begin
declare @giaban money,@mahdb nvarchar(10), @masp nvarchar(10),@slban1 int,@slban2 int
select @mahdb=MaHDB, @masp=MaSP, @slban1=SLBan from inserted
select @mahdb=mahdb, @masp=MaSP, @slban2=SLBan from deleted
select @giaban=GiaBan from dbo.SanPham where MaSP=@masp
update dbo.HoaDonBan set TongTien = isnull(TongTien, 0) + isnull(@giaban*@slban1,0) - isnull(@giaban*@slban2,0) where MaHDB=@mahdb
end

INSERT [dbo].[HoaDonBan] ([MaHDB], [NgayBan], [MaKH], [MaNV],[TongTien]) 
VALUES (N'HDB001',N'2021-10-01 09:20:00' , N'MKH001',N'MNV006',NULL)
INSERT [dbo].[HoaDonBan] ([MaHDB], [NgayBan], [MaKH], [MaNV],[TongTien]) 
VALUES (N'HDB002',N'2021-10-01 10:20:00' , N'MKH002',N'MNV005',NULL)
INSERT [dbo].[HoaDonBan] ([MaHDB], [NgayBan], [MaKH], [MaNV],[TongTien]) 
VALUES (N'HDB003',N'2021-10-01 13:20:00' , N'MKH003',N'MNV001',NULL)
INSERT [dbo].[HoaDonBan] ([MaHDB], [NgayBan], [MaKH], [MaNV],[TongTien]) 
VALUES (N'HDB004',N'2021-10-01 16:20:00' , N'MKH004',N'MNV006',NULL)
INSERT [dbo].[HoaDonBan] ([MaHDB], [NgayBan], [MaKH], [MaNV],[TongTien]) 
VALUES (N'HDB005',N'2021-10-02 09:40:00' , N'MKH005',N'MNV007',NULL)
INSERT [dbo].[HoaDonBan] ([MaHDB], [NgayBan], [MaKH], [MaNV],[TongTien]) 
VALUES (N'HDB006',N'2021-10-02 12:20:00' , N'MKH006',N'MNV002',NULL)
INSERT [dbo].[HoaDonBan] ([MaHDB], [NgayBan], [MaKH], [MaNV],[TongTien]) 
VALUES (N'HDB007',N'2021-10-02 13:20:00' , N'MKH007',N'MNV003',NULL)
INSERT [dbo].[HoaDonBan] ([MaHDB], [NgayBan], [MaKH], [MaNV],[TongTien]) 
VALUES (N'HDB008',N'2021-10-02 15:20:00' , N'MKH008',N'MNV001',NULL)
INSERT [dbo].[HoaDonBan] ([MaHDB], [NgayBan], [MaKH], [MaNV],[TongTien]) 
VALUES (N'HDB009',N'2021-10-03 16:20:00' , N'MKH009',N'MNV005',NULL)
INSERT [dbo].[HoaDonBan] ([MaHDB], [NgayBan], [MaKH], [MaNV],[TongTien]) 
VALUES (N'HDB010',N'2021-10-04 10:40:00' , N'MKH010',N'MNV004',NULL)
INSERT [dbo].[HoaDonBan] ([MaHDB], [NgayBan], [MaKH], [MaNV],[TongTien]) 
VALUES (N'HDB011',N'2021-10-04 13:20:00' , N'MKH011',N'MNV004',NULL)
INSERT [dbo].[HoaDonBan] ([MaHDB], [NgayBan], [MaKH], [MaNV],[TongTien]) 
VALUES (N'HDB012',N'2021-10-05 17:20:00' , N'MKH012',N'MNV002',NULL)
INSERT [dbo].[HoaDonBan] ([MaHDB], [NgayBan], [MaKH], [MaNV],[TongTien]) 
VALUES (N'HDB013',N'2021-10-06 09:20:00' , N'MKH013',N'MNV005',NULL)
INSERT [dbo].[HoaDonBan] ([MaHDB], [NgayBan], [MaKH], [MaNV],[TongTien]) 
VALUES (N'HDB014',N'2021-10-06 11:20:00' , N'MKH014',N'MNV006',NULL)
INSERT [dbo].[HoaDonBan] ([MaHDB], [NgayBan], [MaKH], [MaNV],[TongTien]) 
VALUES (N'HDB015',N'2021-10-06 14:20:00' , N'MKH015',N'MNV007',NULL)
INSERT [dbo].[HoaDonBan] ([MaHDB], [NgayBan], [MaKH], [MaNV],[TongTien]) 
VALUES (N'HDB016',N'2021-10-06 14:30:00' , N'MKH016',N'MNV002',NULL)
INSERT [dbo].[HoaDonBan] ([MaHDB], [NgayBan], [MaKH], [MaNV],[TongTien]) 
VALUES (N'HDB017',N'2021-10-08 12:20:00' , N'MKH017',N'MNV003',NULL)
INSERT [dbo].[HoaDonBan] ([MaHDB], [NgayBan], [MaKH], [MaNV],[TongTien]) 
VALUES (N'HDB018',N'2021-10-09 11:20:00' , N'MKH018',N'MNV001',NULL)
INSERT [dbo].[HoaDonBan] ([MaHDB], [NgayBan], [MaKH], [MaNV],[TongTien]) 
VALUES (N'HDB019',N'2021-10-09 16:20:00' , N'MKH019',N'MNV006',NULL)
INSERT [dbo].[HoaDonBan] ([MaHDB], [NgayBan], [MaKH], [MaNV],[TongTien]) 
VALUES (N'HDB020',N'2021-10-09 19:20:00' , N'MKH020',N'MNV004',NULL)
INSERT [dbo].[HoaDonBan] ([MaHDB], [NgayBan], [MaKH], [MaNV],[TongTien]) 
VALUES (N'HDB021',N'2021-10-09 20:20:00' , N'MKH021',N'MNV002',NULL)
INSERT [dbo].[HoaDonBan] ([MaHDB], [NgayBan], [MaKH], [MaNV],[TongTien]) 
VALUES (N'HDB022',N'2021-10-10 10:20:00' , N'MKH003',N'MNV003',NULL)
INSERT [dbo].[HoaDonBan] ([MaHDB], [NgayBan], [MaKH], [MaNV],[TongTien]) 
VALUES (N'HDB023',N'2021-10-10 14:20:00' , N'MKH006',N'MNV005',NULL)
INSERT [dbo].[HoaDonBan] ([MaHDB], [NgayBan], [MaKH], [MaNV],[TongTien]) 
VALUES (N'HDB024',N'2021-10-10 16:20:00' , N'MKH022',N'MNV001',NULL)
INSERT [dbo].[HoaDonBan] ([MaHDB], [NgayBan], [MaKH], [MaNV],[TongTien]) 
VALUES (N'HDB025',N'2021-10-10 16:20:00' , N'MKH023',N'MNV001',NULL)
INSERT [dbo].[HoaDonBan] ([MaHDB], [NgayBan], [MaKH], [MaNV],[TongTien]) 
VALUES (N'HDB026',N'2021-10-11 13:20:00' , N'MKH024',N'MNV002',NULL)
INSERT [dbo].[HoaDonBan] ([MaHDB], [NgayBan], [MaKH], [MaNV],[TongTien]) 
VALUES (N'HDB027',N'2021-10-12 10:20:00' , N'MKH008',N'MNV004',NULL)
INSERT [dbo].[HoaDonBan] ([MaHDB], [NgayBan], [MaKH], [MaNV],[TongTien]) 
VALUES (N'HDB028',N'2021-10-12 16:30:00' , N'MKH002',N'MNV005',NULL)
INSERT [dbo].[HoaDonBan] ([MaHDB], [NgayBan], [MaKH], [MaNV],[TongTien]) 
VALUES (N'HDB029',N'2021-10-12 16:40:00' , N'MKH016',N'MNV003',NULL)
INSERT [dbo].[HoaDonBan] ([MaHDB], [NgayBan], [MaKH], [MaNV],[TongTien]) 
VALUES (N'HDB030',N'2021-10-12 18:20:00' , N'MKH025',N'MNV007',NULL)
-- chi tiet hdb
create trigger TG_HoaDonNhap_TongSLBan on dbo.ChiTietHDB
for DELETE,INSERT,UPDATE
as
begin
declare @masp nvarchar(10),@slban1 INT,@slban2 INT
IF EXISTS(SELECT * FROM Inserted)
BEGIN
select @masp=MaSP, @slban1=SLBan from inserted
END
IF EXISTS(SELECT * FROM Deleted)
BEGIN
select @masp=MaSP, @slban2=SLBan from deleted
END
update dbo.SanPham set SoLuongBan = isnull(SoLuongBan, 0)+ ISNULL(@slban1,0)- ISNULL(@slban2,0) WHERE dbo.SanPham.MaSP=@masp
end
GO

CREATE TRIGGER TrgUpdateThanhTien ON dbo.ChiTietHDB
FOR INSERT, UPDATE
AS
BEGIN
	declare @mahdb char(10), @masp char(10), @giaban money, @giamgia float
	select @mahdb=MaHDB, @masp=MaSP, @giamgia = GiamGia from inserted
	select @giaban=GiaBan from dbo.SanPham where MaSP=@masp
	update dbo.ChiTietHDB set ThanhTien=@giaban*SLBan-(@giaban*SLBan*(GiamGia/100)) where MaHDB=@mahdb and MaSP=@masp
END


INSERT [dbo].[ChiTietHDB]([MaHDB], [MaSP], [SLBan],[GiamGia],[ThanhTien]) VALUES (N'HDB001', N'SP021',1,10,NULL)
INSERT [dbo].[ChiTietHDB]([MaHDB], [MaSP], [SLBan],[GiamGia],[ThanhTien]) VALUES (N'HDB002', N'SP003',1,10,NULL)
INSERT [dbo].[ChiTietHDB]([MaHDB], [MaSP], [SLBan],[GiamGia],[ThanhTien]) VALUES (N'HDB002', N'SP005',1,10,NULL)
INSERT [dbo].[ChiTietHDB]([MaHDB], [MaSP], [SLBan],[GiamGia],[ThanhTien]) VALUES (N'HDB003', N'SP007',1,7,NULL)
INSERT [dbo].[ChiTietHDB]([MaHDB], [MaSP], [SLBan],[GiamGia],[ThanhTien]) VALUES (N'HDB004', N'SP020',1,0,NULL)
INSERT [dbo].[ChiTietHDB]([MaHDB], [MaSP], [SLBan],[GiamGia],[ThanhTien]) VALUES (N'HDB004', N'SP010',1,0,NULL)
INSERT [dbo].[ChiTietHDB]([MaHDB], [MaSP], [SLBan],[GiamGia],[ThanhTien]) VALUES (N'HDB005', N'SP021',1,2,NULL)
INSERT [dbo].[ChiTietHDB]([MaHDB], [MaSP], [SLBan],[GiamGia],[ThanhTien]) VALUES (N'HDB006', N'SP011',1,0,NULL)
INSERT [dbo].[ChiTietHDB]([MaHDB], [MaSP], [SLBan],[GiamGia],[ThanhTien]) VALUES (N'HDB007', N'SP012',1,0,NULL)
INSERT [dbo].[ChiTietHDB]([MaHDB], [MaSP], [SLBan],[GiamGia],[ThanhTien]) VALUES (N'HDB008', N'SP001',1,3,NULL)
INSERT [dbo].[ChiTietHDB]([MaHDB], [MaSP], [SLBan],[GiamGia],[ThanhTien]) VALUES (N'HDB009', N'SP002',1,0,NULL)
INSERT [dbo].[ChiTietHDB]([MaHDB], [MaSP], [SLBan],[GiamGia],[ThanhTien]) VALUES (N'HDB010', N'SP008',1,0,NULL)
INSERT [dbo].[ChiTietHDB]([MaHDB], [MaSP], [SLBan],[GiamGia],[ThanhTien]) VALUES (N'HDB010', N'SP002',1,0,NULL)
INSERT [dbo].[ChiTietHDB]([MaHDB], [MaSP], [SLBan],[GiamGia],[ThanhTien]) VALUES (N'HDB011', N'SP007',1,5,NULL)
INSERT [dbo].[ChiTietHDB]([MaHDB], [MaSP], [SLBan],[GiamGia],[ThanhTien]) VALUES (N'HDB012', N'SP008',1,5,NULL)
INSERT [dbo].[ChiTietHDB]([MaHDB], [MaSP], [SLBan],[GiamGia],[ThanhTien]) VALUES (N'HDB013', N'SP009',1,0,NULL)
INSERT [dbo].[ChiTietHDB]([MaHDB], [MaSP], [SLBan],[GiamGia],[ThanhTien]) VALUES (N'HDB014', N'SP006',1,7,NULL)
INSERT [dbo].[ChiTietHDB]([MaHDB], [MaSP], [SLBan],[GiamGia],[ThanhTien]) VALUES (N'HDB015', N'SP004',1,0,NULL)
INSERT [dbo].[ChiTietHDB]([MaHDB], [MaSP], [SLBan],[GiamGia],[ThanhTien]) VALUES (N'HDB016', N'SP002',1,9,NULL)
INSERT [dbo].[ChiTietHDB]([MaHDB], [MaSP], [SLBan],[GiamGia],[ThanhTien]) VALUES (N'HDB017', N'SP001',1,0,NULL)
INSERT [dbo].[ChiTietHDB]([MaHDB], [MaSP], [SLBan],[GiamGia],[ThanhTien]) VALUES (N'HDB018', N'SP022',1,0,NULL)
INSERT [dbo].[ChiTietHDB]([MaHDB], [MaSP], [SLBan],[GiamGia],[ThanhTien]) VALUES (N'HDB019', N'SP023',1,10,NULL)
INSERT [dbo].[ChiTietHDB]([MaHDB], [MaSP], [SLBan],[GiamGia],[ThanhTien]) VALUES (N'HDB020', N'SP025',1,15,NULL)
INSERT [dbo].[ChiTietHDB]([MaHDB], [MaSP], [SLBan],[GiamGia],[ThanhTien]) VALUES (N'HDB021', N'SP021',2,0,NULL)
INSERT [dbo].[ChiTietHDB]([MaHDB], [MaSP], [SLBan],[GiamGia],[ThanhTien]) VALUES (N'HDB022', N'SP021',1,0,NULL)
INSERT [dbo].[ChiTietHDB]([MaHDB], [MaSP], [SLBan],[GiamGia],[ThanhTien]) VALUES (N'HDB023', N'SP004',1,0,NULL)
INSERT [dbo].[ChiTietHDB]([MaHDB], [MaSP], [SLBan],[GiamGia],[ThanhTien]) VALUES (N'HDB024', N'SP013',1,0,NULL)
INSERT [dbo].[ChiTietHDB]([MaHDB], [MaSP], [SLBan],[GiamGia],[ThanhTien]) VALUES (N'HDB025', N'SP016',1,0,NULL)
INSERT [dbo].[ChiTietHDB]([MaHDB], [MaSP], [SLBan],[GiamGia],[ThanhTien]) VALUES (N'HDB026', N'SP019',1,10,NULL)
INSERT [dbo].[ChiTietHDB]([MaHDB], [MaSP], [SLBan],[GiamGia],[ThanhTien]) VALUES (N'HDB027', N'SP024',1,0,NULL)
INSERT [dbo].[ChiTietHDB]([MaHDB], [MaSP], [SLBan],[GiamGia],[ThanhTien]) VALUES (N'HDB028', N'SP013',1,0,NULL)
INSERT [dbo].[ChiTietHDB]([MaHDB], [MaSP], [SLBan],[GiamGia],[ThanhTien]) VALUES (N'HDB028', N'SP012',1,0,NULL)
INSERT [dbo].[ChiTietHDB]([MaHDB], [MaSP], [SLBan],[GiamGia],[ThanhTien]) VALUES (N'HDB029', N'SP010',1,5,NULL)
INSERT [dbo].[ChiTietHDB]([MaHDB], [MaSP], [SLBan],[GiamGia],[ThanhTien]) VALUES (N'HDB029', N'SP009',1,0,NULL)
INSERT [dbo].[ChiTietHDB]([MaHDB], [MaSP], [SLBan],[GiamGia],[ThanhTien]) VALUES (N'HDB029', N'SP003',1,5,NULL)
INSERT [dbo].[ChiTietHDB]([MaHDB], [MaSP], [SLBan],[GiamGia],[ThanhTien]) VALUES (N'HDB030', N'SP022',2,4,NULL)

